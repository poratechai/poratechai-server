const user = require('../models/user');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const randomstring = require("randomstring");
const smsVerification = require('./smsVerification');
const checkToken = require('./checkToken');
const config = require('../config/config.json');

const changePassword = (email, password, newPassword) =>

	new Promise((resolve, reject) => {

		user.find({ email: email })

		.then(users => {

			let user = users[0];
			const hashed_password = user.hashedPassword;

			if (bcrypt.compareSync(password, hashed_password)) {

				const salt = bcrypt.genSaltSync(10);
				const hash = bcrypt.hashSync(newPassword, salt);

				user.hashedPassword = hash;

				return user.save();

			} else {

				reject({ status: 401, message: 'Invalid Old Password !' });
			}
		})

		.then(user => resolve({ status: 200, message: 'Password Updated Sucessfully !' }))

		.catch(err => reject({ status: 500, message: 'Internal Server Error !' }));

	});

exports.changePassword = changePassword;

const resetPasswordInit = contactNumber =>{
	return new Promise((resolve, reject) => {

		const random = randomstring.generate(6);

		user.findOne({ contactNumber: contactNumber })

		.then(user => {
			if (!user) {
				reject({ status: 404, message: 'User Not Found !' });
			} else {
				const salt = bcrypt.genSaltSync(10);
				const hash = bcrypt.hashSync(random, salt);

				user.hashedPassword = hash;
				// user.temp_password_time = new Date();

				return user.save();
			}
		})

		.then(user => {
			if(user){
				smsVerification.sendNewPassword(contactNumber, random)
				.then(function (result) {
					if(result){
						resolve({ status: 200, message: 'You will get an sms shortly'});
					}
				})
				.catch(function (err) {
					reject({ status: 400, message: 'Please try again !' });
				});
			}
		})
		.catch(err => {
			console.log(err);
			reject({ status: 500, message: 'Internal Server Error !' });
		});
	});
}

exports.resetPasswordInit = resetPasswordInit;

const resetPasswordFinish = (email, token, password) =>

	new Promise((resolve, reject) => {

		user.find({ email: email })

		.then(users => {

			let user = users[0];

			const diff = new Date() - new Date(user.temp_password_time);
			const seconds = Math.floor(diff / 1000);
			console.log(`Seconds : ${seconds}`);

			if (seconds < 120) { return user; } else { reject({ status: 401, message: 'Time Out ! Try again' }); } }) .then(user => {

			if (bcrypt.compareSync(token, user.temp_password)) {

				const salt = bcrypt.genSaltSync(10);
				const hash = bcrypt.hashSync(password, salt);
				user.hashed_password = hash;
				user.temp_password = undefined;
				user.temp_password_time = undefined;

				return user.save();

			} else {

				reject({ status: 401, message: 'Invalid Token !' });
			}
		})

		.then(user => resolve({ status: 200, message: 'Password Changed Successfully !' }))

		.catch(err => reject({ status: 500, message: 'Internal Server Error !' }));

	});

exports.resetPasswordFinish = resetPasswordFinish;

exports.changePasswordRouterFunction = (req,res) =>{
	if (checkToken.checkToken(req)) {

		const oldPassword = req.body.oldPassword;
		const newPassword = req.body.newPassword;

		if (!oldPassword || !newPassword || !oldPassword.trim() || !newPassword.trim()) {

			res.status(400).json({ message: 'Invalid Request !' });

		} else {

			changePassword(req.headers['email-address'], oldPassword, newPassword)

			.then(result => res.status(result.status).json({ message: result.message }))

			.catch(err => res.status(err.status).json({ message: err.message }));

		}
	} else {

		res.status(401).json({ message: 'Invalid Token !' });
	}
};

exports.resetPasswordRouterFunction = (req, res) =>{
	const contactNumber = '0'+req.body.contactNumber.toString();

	if (contactNumber) {
		resetPasswordInit(contactNumber)
		.then(result => res.status(result.status).json({ message: result.message }))
		.catch(err => res.status(err.status).json({ message: err.message }));
	} else {
		res.status(400).json({ message: 'Contact number cannot be empty'});
	}
};
