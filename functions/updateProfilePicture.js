const user = require('../models/user');
const checkToken = require('./checkToken');

const updateProfilePicture = (userId,profilePicture) =>

	new Promise((resolve,reject) => {

		user.findOneAndUpdate({ _id: mongoose.Types.ObjectId(userId) }, { profilePicture: profilePicture})

		.then(() => resolve({ status: 200, message: 'Picture updated Sucessfully !' }))

		.catch(err => reject({ status: 500, message: 'Internal Server Error Fuck !' }))

	});

exports.updateProfilePicture = updateProfilePicture;


exports.routerFunction=(req,res)=>{
	if(checkToken.checkToken(req)){
		const profilePicture = req.body.profilePicture;

		updateProfilePicture(req.headers['user-id'],profilePicture)

	.then(result => res.json(result))

	.catch(err => res.status(err.status).json({ message: err.message }));
}
	else{
		res.status(401).json({ message: 'Invalid Token !' });
	}
};
