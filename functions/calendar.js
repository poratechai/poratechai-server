let Calendar = require('../models/calendar');
let mongoose = require('mongoose');

exports.createCalendar = function(job_id){
    let calendar = new Calendar({
        jobId       : mongoose.Types.ObjectId(job_id),
        createdAt   : new Date()
    });

    calendar.save((err, savedCalendarDate) => {
        if(err){
            console.log("Error creating Calendar date for job - ", job_id);
        }else{

        }
    });
};

exports.getJobCalendarMonth = function(req, res){
    let job_id = req.query.jobId;

    Calendar.findOne({
        'jobId' : mongoose.Types.ObjectId(job_id)
    })
        .exec()
        .then((jobCalendarMonth) => {
            let filteredJobCalendarDates = jobCalendarMonth.dates.filter((value, index, arr) => {
                return (value.year === parseInt(req.query.year)) && (value.month === parseInt(req.query.month))
            });

            jobCalendarMonth.dates = filteredJobCalendarDates;

            res.json(jobCalendarMonth);
        })
        .catch((err) => {
            console.log(err);
        });
};

exports.parentStudentSignature = function(req, res){
    let job_id = req.query.jobId;

    Calendar.findOne({
        'jobId'     : mongoose.Types.ObjectId(job_id),
    }).exec()
        .then((jobCalendar) => {

            let dateIndex = findDateEntry(jobCalendar, req.query.year, req.query.month, req.query.day);
            let dateEntry;

            if(dateIndex === -1){
                dateEntry = createDateEntry(jobCalendar, req.body.sign==='true', false, "", "", req.query.year, req.query.month, req.query.day);
                jobCalendar.dates.push(dateEntry);
            }else{
                dateEntry = jobCalendar.dates[dateIndex];
                if(dateEntry.parentStudentSignature === true && dateEntry.tutorSignature === true){
                    res.json("Attendance is locked for this day");
                    return;
                }else{
                    jobCalendar.dates[dateIndex].parentStudentSignature = req.body.sign === 'true';
                }
            }

            jobCalendar.save((err) => {
                if(err) console.log(err);
                else {
                    res.json(dateEntry);
                }
            });
        })
};

exports.tutorSignature = function(req, res){
    let job_id = req.query.jobId;

    Calendar.findOne({
        'jobId'     : mongoose.Types.ObjectId(job_id),
    }).exec()
        .then((jobCalendar) => {

            let dateIndex = findDateEntry(jobCalendar, req.query.year, req.query.month, req.query.day);
            let dateEntry;

            if(dateIndex === -1){
                dateEntry = createDateEntry(jobCalendar, false, req.body.sign===true, "", "", req.query.year, req.query.month, req.query.day);
                jobCalendar.dates.push(dateEntry);
            }else {
                dateEntry = jobCalendar.dates[dateIndex];
                if(dateEntry.parentStudentSignature === true && dateEntry.tutorSignature === true){
                    res.json("Attendance is locked for this day");
                    return;
                }else{
                    jobCalendar.dates[dateIndex].tutorSignature = req.body.sign === 'true';
                }
            }

            jobCalendar.save((err) => {
                if(err) console.log(err);
                else {
                    res.json(dateEntry);
                }
            });
        })
};

exports.parentStudentNote = function(req, res){
    let job_id = req.query.jobId;

    Calendar.findOne({
        'jobId'     : mongoose.Types.ObjectId(job_id),
    }).exec()
        .then((jobCalendar) => {

            let dateIndex = findDateEntry(jobCalendar, req.query.year, req.query.month, req.query.day);
            let dateEntry;

            if(dateIndex === -1){
                dateEntry = createDateEntry(jobCalendar, false, false, req.body.notes, "", req.query.year, req.query.month, req.query.day);
                jobCalendar.dates.push(dateEntry);
            }else {
                dateEntry = jobCalendar.dates[dateIndex];
                jobCalendar.dates[dateIndex].parentStudentNote = req.body.notes;
            }

            jobCalendar.save((err) => {
                if(err) console.log(err);
                else {
                    res.json(dateEntry);
                }
            });
        })
};

exports.tutorNote = function(req, res){
    let job_id = req.query.jobId;

    Calendar.findOne({
        'jobId'     : mongoose.Types.ObjectId(job_id),
    }).exec()
        .then((jobCalendar) => {

            let dateIndex = findDateEntry(jobCalendar, req.query.year, req.query.month, req.query.day);
            let dateEntry;

            if(dateIndex === -1){
                dateEntry = createDateEntry(jobCalendar, false, false, "", req.body.notes, req.query.year, req.query.month, req.query.day);
                jobCalendar.dates.push(dateEntry);
            }else {
                dateEntry = jobCalendar.dates[dateIndex];
                jobCalendar.dates[dateIndex].tutorNote = req.body.notes;
            }

            jobCalendar.save((err) => {
                if(err) console.log(err);
                else {
                    res.json(dateEntry);
                }
            });
        })
};

findDateEntry = function(jobCalendar, year, month, day){
    for(let i = 0; i < jobCalendar.dates.length; i++){
        if(jobCalendar.dates[i].year === parseInt(year) &&
            jobCalendar.dates[i].month === parseInt(month) && jobCalendar.dates[i].day === parseInt(day)){

            return i;
        }
    }

    return -1;
};

createDateEntry = function(jobCalendar, parentStudentSignature, tutorSignature, parentStudentNote, tutorNote, year, month, day){
    let dateEntry = {
        'parentStudentSignature'    : parentStudentSignature,
        'tutorSignature'            : tutorSignature,
        'parentStudentNote'         : parentStudentNote,
        'tutorNote'                 : tutorNote,
        'year'                      : year,
        'month'                     : month,
        'day'                       : day
    };

    return dateEntry;
};