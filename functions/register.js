const user = require('../models/user');
const bcrypt = require('bcryptjs');
let Promise = require('bluebird');
let mongoose = require('mongoose');
mongoose.Promise = Promise;
const smsVerification = require('./smsVerification');
const walletFunctionImported = require('../functions/wallet');

const registerUser = (fullName, contactNumber, profilePicture, gender) =>
    new Promise((resolve, reject) => {
        // const salt = bcrypt.genSaltSync(10);
        // const hash = bcrypt.hashSync(password, salt);

        const newUser = new user({
            fullName: fullName,
            // email: email,
            contactNumber: contactNumber,
            // hashedPassword: hash,
            gender: gender,
            profilePicture: profilePicture,
            createdAt: new Date()
        });

        //Ratul - start
        let promiseUser = newUser.save();
        let promiseWallet;

        promiseUser
            .then(userData => {
                console.log('User Created Successfully - ' + userData.id);

                promiseWallet = walletFunctionImported.createWallet(
                    userData.id
                );
            })
            .then(() => {
                return Promise.all([promiseUser, promiseWallet]).then(
                    ([userData, walletData]) => {
                        user.update(
                            { _id: userData._id },
                            {
                                $set: {
                                    walletId: walletData._id,
                                    couponId: mongoose.Types.ObjectId(
                                        '5c069c0675cf130fa0c0b754'
                                    )
                                }
                            }
                        ).exec();

                        console.log('Wallet added to User');

                        return userData;
                    }
                );
            })
            .then(user =>
                resolve({
                    status: 201,
                    message: 'User Registered Successfully !',
                    userId: user._id,
                    contactNumber: user.contactNumber
                })
            )
            .catch(err => {
                console.log(err);

                if (err.code == 11000) {
                    reject({
                        status: 409,
                        message: 'User Already Registered !'
                    });
                } else {
                    reject({ status: 500, message: 'Internal Server Error !' });
                }
            });
        //Ratul - end
    });

exports.registerUser = registerUser;

exports.routerFunction = (req, res) => {
    const name = req.body.fullName;
    // const email = req.body.email;
    // const password = req.body.password;
    const contactNumber = req.body.contactNumber;
    const profilePicture = req.body.profilePicture;
    const gender = req.body.gender;

    if (
        !name ||
        !contactNumber ||
        !profilePicture ||
        !gender ||
        !name.trim() ||
        !gender.trim() ||
        !profilePicture.trim()
    ) {
        res.status(400).json({ message: 'Bad request' });
    } else {
        registerUser(name, contactNumber, profilePicture, gender)
            .then(result => {
                // res.setHeader('Location', '/users/'+ email);
                res.status(result.status).json({
                    message: result.message,
                    userId: result.userId.toString(),
                    contactNumber: result.contactNumber
                });
                // smsVerification.sendCode(result.userId, result.contactNumber);
            })

            .catch(err => {
                console.log('err', err);
                res.status(err.status).json({ message: err.message });
            });
    }
};
