const { peekCache, peekCacheKey } = require('../functions/constants');

exports.getPeekResults = function(req, res){
    peekCache.get(peekCacheKey, function(err, peekResults){
        if(err){
            console.log("Error retrieving peek cache results - " + err);
            res.status(500).send("Error");
        }else{
            res.json({message: peekResults});
        }
    });
};