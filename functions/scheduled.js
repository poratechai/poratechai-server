let Match = require('../models/match');
let User = require('../models/user');
let Job = require('../models/job');
let DateDiff = require('date-diff');
let match = require('../functions/match');
let Promise = require('bluebird');
let mongoose = Promise.promisifyAll(require('mongoose'));
let pythonShell = require('python-shell');

const Notification = require('./notifications/notification');

const util = require('util');

//Matching - start

let dateDiff = 0;
let presentDate = new Date();

exports.optimizeMatchModelV1 = function()
{
    console.log('here');

    //General Information
    let dateDiff = 0;
    let presentDate = new Date("2018-01-10T00:00:00.000Z");

    //60 days check
    Match.find( {
            previousStatus : { $lte : 1 },
            currentStatus : { $lte : 2 }
        },
        function(err, result){
          if(err)
          {
            console.log(err);
          }
          else
          {
              let len = result.length;

              for(let i = 0; i < len; i++)
              {
                  dateDiff = Date.diff(presentDate, result[i].dateOfStatusChange);

                  if(dateDiff.days() > 60)
                  {
                      result[i].remove(function(err){
                          if(err)
                          {
                              console.log(err);
                          }
                          else
                          {
                              console.log("Removed");
                          }
                      });
                  }
              }

              //14 days check
              Match.find( {
                      previousStatus : 2,
                      currentStatus : 3
                  },
                  function(err, result){
                      if(err)
                      {
                          console.log(err);
                      }
                      else
                      {
                          let len = result.length;

                          for(let i = 0; i < len; i++)
                          {
                              dateDiff = Date.diff(presentDate, result[i].dateOfStatusChange);

                              console.log(dateDiff.days());

                              if(dateDiff.days() > 14)
                              {
                                  result[i].set({
                                      currentStatus : 4,
                                      previousStatus : 3
                                  });

                                  result[i].save(function(err, updatedValue){
                                      if(err)
                                      {
                                          console.log(err);
                                      }
                                      else
                                      {
                                          console.log("Updated");
                                      }
                                  });
                              }
                          }

                          //3,4 check
                          Match.find( {
                                  previousStatus : 3,
                                  currentStatus : 4
                              },
                              function(err, result){
                                  if(err)
                                  {
                                      console.log(err);
                                  }
                                  else
                                  {
                                      let len = result.length;

                                      for(let i = 0; i < len; i++)
                                      {
                                          User.findByIdAndUpdate(result[i].tutorId,
                                              {
                                                  $push: {
                                                      jobHistory: {
                                                          jobId: result[i].jobId,
                                                          status: 4
                                                      }
                                                  }
                                              },
                                              function(err, num){
                                                  if(err) {
                                                      console.log(err);
                                                  }
                                                  else {
                                                      console.log('User update - ' + num);
                                                  }
                                              });

                                          Job.findByIdAndUpdate(result[i].jobId,
                                              {
                                                  tutorId : result[i].tutorId
                                              },
                                              function(err, num){
                                                  if(err){
                                                      console.log(err);
                                                  }
                                                  else {
                                                      console.log('Job update - ' + num);
                                                  }});

                                          Match.findByIdAndRemove(result[i]._id,
                                              function(err, num){
                                                  if(err){
                                                      console.log(err);
                                                  }
                                                  else{
                                                      console.log('Removed - ' + num);
                                                  }
                                              });
                                      }
                                  }
                              });
                      }
                  });
          }
    });
};

exports.optimizeMatchModelV2 = function()
{
    check60()
        .then(check14)
        .catch(function(err){
        console.log(err);
    })
};

const check60 = function()
{
    //60 days check
    promise60 = Match.find( {
        previousStatus : { $lte : 1 },
        currentStatus : { $lte : 2 }
    }).exec();

    promise60.then(function(result){
        let len = result.length;

        for(let i = 0; i < len; i++)
        {
            dateDiff = Date.diff(presentDate, result[i].dateOfStatusChange);

            if(dateDiff.days() > 60)
            {
                result[i].remove(function(err){
                    if(err)
                    {
                        console.log(err);
                    }
                    else
                    {
                        console.log("Removed");
                    }
                });
            }
        }
    });

    return promise60;
};

const check14 = function()
{
    //14 days check
    let notificationData = {
        deviceIdToken: '',
        jobId: '',
        matchId: '',
        jobTitle: '',
        tutorId: '',
        parentId: ''
    };

    let promise14 = Match.find({
        previousStatus : 2,
        currentStatus : 3
        })
        .populate({
            path: 'tutorProfile'
        })
        .populate({
            path: 'jobData',
            populate: {
                path: 'userProfile'
            }
        })
        .exec();

    let promises23to34 = [];

    promise14.then(function(result){
        let len = result.length;

        for(let i = 0; i < len; i++)
        {
            dateDiff = Date.diff(presentDate, result[i].dateOfStatusChange);

            console.log("Date diff - days: " + dateDiff.days());

            if(dateDiff.days() > 14)
            {
                console.log("14 Days Entered");

                result[i].set({
                    currentStatus : 4,
                    previousStatus : 3
                });

                let promiseSet = result[i].save(function(err, updated){
                    if(err) console.log(err);
                });

                console.log("14 Days Scheduled - ");

                console.log(result[i]);

                try {
                    //call notification
                    notificationData.jobId = result[i].jobId;
                    notificationData.matchId = result[i]._id;
                    notificationData.jobTitle = result[i].jobData[0].jobTitle;
                    notificationData.tutorId = result[i].tutorId;
                    notificationData.parentId = result[i].jobData[0].userProfile[0]._id;

                    notificationData.deviceIdToken = result[i].jobData[0].userProfile[0].deviceIdToken;
                    Notification.notificationAfter14DaysOfInspectionToParent(notificationData);

                    notificationData.deviceIdToken = result[i].tutorProfile[0].deviceIdToken;
                    Notification.notificationAfter14DaysOfInspectionToTutor(notificationData);
                }catch(e){
                    console.log(e);
                }

                promises23to34.push(promiseSet);
            }
        }

        Promise.all(promises23to34).then(check34);
    });
};

const check34 = function()
{
    //3,4 check
    promise34 = Match.find( {
        previousStatus : 3,
        currentStatus : 4
    }).exec();

    promise34.then(function(result) {
        let len = result.length;

        try
        {
            for(let i = 0; i < len; i++)
            {
                User.update({
                        _id : result[i].tutorId
                    },
                    {
                        $push: {
                            jobHistory: result[i].jobId
                        }
                    },
                    function(err, num){
                        if(err) {
                            console.log(err.message);
                        }
                        else {
                            console.log('User update - ' + num);
                        }
                    });

                Job.update({
                        _id : result[i].jobId
                    },
                    {
                        tutorId : result[i].tutorId,
                        status : 4
                    },
                    function(err, num){
                        if(err){
                            console.log(err);
                        }
                        else {
                            console.log('Job update - ' + num);
                        }});

                Match.remove({
                        jobId : result[i].jobId
                    },
                    function(err, num){
                        if(err){
                            console.log(err);
                        }
                        else{
                            console.log('Removed - ' + num);
                        }
                    });
            }
        }
        catch(e)
        {
            console.log(e);
        }
    });
};

exports.checkNewMatches = function(){
    let promiseNewMatches = Match.find( { notified: 0, currentStatus: { $gte: 0 } } )
        .populate({
            path: 'jobData'
        })
        .populate({
            path: 'tutorProfile'
        })
        .exec();

    promiseNewMatches
        .then(matchNotification)
        .then(updateMatchNotified)
        .catch(function(err){
            console.log(err);
    })
};

const matchNotification = function(data)
{
    console.log("New matches: " + data);

    let jobData = {
        deviceIdToken: '',
        matchId: '',
        jobTitle: '',
    }

    for(let i=0; i<data.length; i++){
        jobData.deviceIdToken = data[i].tutorProfile[0].deviceIdToken;
        jobData.matchId = data[i]._id;
        jobData.jobTitle = data[i].jobData[0].jobTitle;

        Notification.notifyTutorAboutNewMatch(jobData);
    }
};

const updateMatchNotified = function(result)
{
    Match.update(
        { notified: 0 },
        { $set: { notified: 1 } },
        { multi: true})
        .exec(
            function(err, numOfMatches){
                if(err) console.log(err);
                else {
                    console.log("Match Updated Successfully! - "+  numOfMatches);
                }
            }
        )
};

exports.test = function()
{

  Match.remove({}, function(err){
      if(err){
          console.log(err)
      }
      else{
          console.log("Removed Match documents");
      }
  });

  Job.update({}, {$unset : {tutorId : "", status : ""}}, {multi : true}, function(err){
      if(err){
          console.log(err);
      }
      else{
          console.log("TutorId removed");
      }
  });

  User.update({}, {$set : {jobHistory : []}}, {multi : true}, function(err){
      if(err){
          console.log(err);
      }
      else{
          console.log("Job History removed");
      }
  });
    {
        match.createDummyMatch("5a587a52224d1d08cc473235", "5a587c916b3ee518e8615b7c", 0, 0, new Date("2017-11-01T00:00:00.000Z"));
        match.createDummyMatch("5a587a52224d1d08cc473235", "5a587d1a6b3ee518e8615b7e", 0, 1, new Date("2017-11-01T00:00:00.000Z"));
        match.createDummyMatch("5a587a52224d1d08cc473235", "5a587d1a6b3ee518e8615b7e", 2, 3, new Date("2018-01-02T00:00:00.000Z"));
        match.createDummyMatch("5a587a52224d1d08cc473235", "5a587d276b3ee518e8615b80", 1, 2, new Date("2017-11-01T00:00:00.000Z"));
        match.createDummyMatch("5a587a65224d1d08cc473236", "5a587bfc6b3ee518e8615b76", 2, 3, new Date("2017-12-28T00:00:00.000Z"));
        match.createDummyMatch("5a587a52224d1d08cc473235", "5a587bfc6b3ee518e8615b76", 0, 1, new Date("2017-12-28T00:00:00.000Z"));
        match.createDummyMatch("5a587a52224d1d08cc473235", "5a587bfc6b3ee518e8615b76", 1, 2, new Date("2017-12-28T00:00:00.000Z"));
        match.createDummyMatch("5a587a65224d1d08cc473236", "5a587c356b3ee518e8615b78", 3, 4, new Date("2017-12-28T00:00:00.000Z"));
        match.createDummyMatch("5a587a65224d1d08cc473236", "5a587c3d6b3ee518e8615b7a", 2, 3, new Date("2017-12-28T00:00:00.000Z"));
    }
};

exports.runPythonMatching = function()
{
  let options = {
    mode: 'text',
    pythonOptions: ['-u'],
    scriptPath: __dirname+'/matching',
  };

  pythonShell.run('mongopython.py', options, function (err, results) {
    if (err) throw err;
    // results is an array consisting of messages collected during execution
    console.log("Called Python Matching");
  });
};

//Matching - end

//Peek - start

const fs = require('fs');
const fileName = './functions/matching/system_constants';

const { peekCache, peekCacheKey } = require('../functions/constants');

exports.generatePeekResults = function()  //cache peek stats
{
    let system_constants = new Promise(function(resolve, reject){
        fs.readFile(fileName, 'utf8', function(err, data){
            if(err){
                console.log(err);
                reject(err);
            }else{
                // console.log(data);
                resolve(data);
            }
        });
    });

    system_constants.then((data) => {
        const peekKeys = JSON.parse(data);

        const peekResults = {
            'classesToTeach': {},
            'subcategories': {},
            'subjects': {}
        };
        initializePeekResults(peekKeys, peekResults);

        peekIntoAllJobs(peekKeys, peekResults)
            .then(() => {
                return peekIntoAllTutors(peekKeys, peekResults);
            })
            .then(() => {
                // console.log(util.inspect(peekResults, {showHidden: false, depth: null}));
                cachePeekResults(peekResults);
            });
    });
};

const initializePeekResults = function(peekKeysOriginal, peekResults)
{
    for(let i = 0; i < peekKeysOriginal.classesToTeach.length; i++){
        peekResults['classesToTeach'][peekKeysOriginal['classesToTeach'][i]] = {};
    }

    let subcategoriesKeys = Object.keys(peekKeysOriginal['subcategories']);
    for(let i = 0; i < subcategoriesKeys.length; i++){
        peekResults['subcategories'][subcategoriesKeys[i]] = {};
        for(let j = 0; j < peekKeysOriginal['subcategories'][subcategoriesKeys[i]].length; j++){
            peekResults['subcategories'][subcategoriesKeys[i]][peekKeysOriginal['subcategories'][subcategoriesKeys[i]][j]] = {};
        }
    }

    for(let i = 0; i < peekKeysOriginal.subjects.length; i++){
        peekResults['subjects'][peekKeysOriginal['subjects'][i]] = {};
    }
};

const peekIntoAllTutors = function(peekKeysOriginal, peekResults)
{
    let promiseToCompletePeekIntoAllTutors = [];

    //classes to teach
    let promiseToPeekIntoClassesToTeach = [];
    for(let i = 0; i < peekKeysOriginal['classesToTeach'].length; i++){
        promiseToPeekIntoClassesToTeach.push(
            User.count({
                "tutorUpdateFlag": 'true',
                "preferredClassToTeach": peekKeysOriginal['classesToTeach'][i]
            })
        );
    }
    promiseToCompletePeekIntoAllTutors.push(new Promise((resolve, reject) => {
        Promise.all(promiseToPeekIntoClassesToTeach).then((results) => {
            for(let i = 0; i < results.length; i++){
                peekResults['classesToTeach'][peekKeysOriginal['classesToTeach'][i]]['tutors'] = results[i];
            }
            resolve();
        });
    }));

    //subcategories
    let promiseToPeekIntoSubcategories = [];
    let categoriesKeys = Object.keys(peekKeysOriginal['subcategories']);
    promiseToCompletePeekIntoAllTutors.push(new Promise((resolve, reject) => {
        for(let i = 0; i < categoriesKeys.length; i++){
            for(let j = 0; j < peekKeysOriginal['subcategories'][categoriesKeys[i]].length; j++){
                promiseToPeekIntoSubcategories.push(
                    User.count({
                        "tutorUpdateFlag": 'true',
                        "preferredSubcategory": peekKeysOriginal['subcategories'][categoriesKeys[i]][j]
                    })
                );
            }

            Promise.all(promiseToPeekIntoSubcategories).then((results) => {
                for(let j = 0; j < results.length; j++){
                    peekResults['subcategories'][categoriesKeys[i]][peekKeysOriginal['subcategories'][categoriesKeys[i]][j]]['tutors'] = results[j];
                }
            });

            promiseToPeekIntoSubcategories = [];
        }
        resolve();
    }));

    //subjects
    let promiseToPeekIntoSubjects = [];
    for(let i = 0; i < peekKeysOriginal['subjects'].length; i++){
        promiseToPeekIntoSubjects.push(
            User.count({
                "tutorUpdateFlag": 'true',
                "preferredSubjects": peekKeysOriginal['subjects'][i]
            })
        );
    }
    promiseToCompletePeekIntoAllTutors.push(new Promise((resolve, reject) => {
        Promise.all(promiseToPeekIntoSubjects).then((results) => {
            let peekSubjects = peekKeysOriginal['subjects'].slice();
            for (let i = 0; i < results.length; i++) {
                peekResults['subjects'][peekKeysOriginal['subjects'][i]]['tutors'] = results[i];
            }
            resolve();
        })
    }));

    return Promise.all(promiseToCompletePeekIntoAllTutors);
};

const peekIntoAllJobs = function(peekKeysOriginal, peekResults)
{
    let promiseToCompletePeekIntoAllJobs = [];

    //classes to teach
    let promiseToPeekIntoClassesToTeach = [];
    for(let i = 0; i < peekKeysOriginal['classesToTeach'].length; i++){
        promiseToPeekIntoClassesToTeach.push(
            Job.count({
                "class": peekKeysOriginal['classesToTeach'][i]
            })
        );
    }
    promiseToCompletePeekIntoAllJobs.push(new Promise((resolve, reject) => {
        Promise.all(promiseToPeekIntoClassesToTeach).then((results) => {
            for(let i = 0; i < results.length; i++){
                peekResults['classesToTeach'][peekKeysOriginal['classesToTeach'][i]]['jobs'] = results[i];
            }
            resolve();
        });
    }));

    //subcategories
    let promiseToPeekIntoSubcategories = [];
    let categoriesKeys = Object.keys(peekKeysOriginal['subcategories']);
    promiseToCompletePeekIntoAllJobs.push(new Promise((resolve, reject) => {
        for(let i = 0; i < categoriesKeys.length; i++){
            for(let j = 0; j < peekKeysOriginal['subcategories'][categoriesKeys[i]].length; j++){
                promiseToPeekIntoSubcategories.push(
                    Job.count({
                        "subcategory": peekKeysOriginal['subcategories'][categoriesKeys[i]][j]
                    })
                );
            }

            Promise.all(promiseToPeekIntoSubcategories).then((results) => {
                for(let j = 0; j < results.length; j++){
                    peekResults['subcategories'][categoriesKeys[i]][peekKeysOriginal['subcategories'][categoriesKeys[i]][j]]['jobs'] = results[j];
                }
            });

            promiseToPeekIntoSubcategories = [];
        }
        resolve();
    }));

    //subjects
    let promiseToPeekIntoSubjects = [];
    for(let i = 0; i < peekKeysOriginal['subjects'].length; i++){
        promiseToPeekIntoSubjects.push(
            Job.count({
                "subjects": peekKeysOriginal['subjects'][i]
            })
        );
    }
    promiseToCompletePeekIntoAllJobs.push(new Promise((resolve, reject) => {
        Promise.all(promiseToPeekIntoSubjects).then((results) => {
            for (let i = 0; i < results.length; i++) {
                peekResults['subjects'][peekKeysOriginal['subjects'][i]]['jobs'] = results[i];
            }
            resolve();
        })
    }));

    return Promise.all(promiseToCompletePeekIntoAllJobs);
};

const cachePeekResults = function(peekResults)
{
    console.log(peekCacheKey);
    peekCache.set(peekCacheKey, JSON.stringify(peekResults));
};

//Peek - end
