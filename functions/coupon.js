let User = require('../models/user');
let Coupon = require('../models/coupon');
let mongoose = require('mongoose');

let defaultCoupon = "5c52e80ee297910011e20b33";             //0% coupon
let registrationDefaultCoupon = "5c069c0675cf130fa0c0b754"; //100% coupon

exports.createCoupon = function(req, res){
    let coupon = new Coupon({
        couponTitle         : req.body.couponTitle,
        couponDescription   : req.body.couponDescription,
        oneMonth            : req.body.oneMonth,
        threeMonths         : req.body.threeMonths,
        validTill           : req.body.validTill
    });

    coupon.save(function(err, couponData){
        if(err){
            console.log(err);
        }else{
            console.log("Coupon created");
        }
    });
};

exports.addCouponToUser = function(req, res){
    User.update({
        _id : req.headers['user-id']
    }, {
        couponId    : req.body.couponId
    }).exec(function(err, couponData){
        if(err){
            console.log(err);
        }else{

        }
    });
};

exports.addDefaultCouponToAllUsers = function(){
    User.update({

    }, {
        $set    : { 'couponId' : mongoose.Types.ObjectId(registrationDefaultCoupon) }
    }, {
        multi   : true
    }, function(err, res){

    });
};

exports.getCoupons = function(req, res){
    Coupon.find({

    }).exec(function(err, coupons){
        if(err){
            console.log(err);
        }else{
            res.json(coupons);
        }
    });
};

exports.getCouponsForDeletion = function(req, res){
    Coupon.find({
        _id: { $nin: [mongoose.Types.ObjectId(defaultCoupon)] }
    }).exec(function(err, coupons){
        if(err){
            console.log(err);
        }else{
            res.json(coupons);
        }
    });
};

exports.deleteCoupons = function(req, res){
    let couponIds = req.body.couponIds;

    let couponObjectIds = couponIds.map(function(val){
        return mongoose.Types.ObjectId(val);
    });

    User.update({
        couponId: { $in: couponObjectIds }
    }, {
        $set: { couponId: mongoose.Types.ObjectId(defaultCoupon) }
    }, {
        multi: true
    }).exec(function(err){
        if(err){
            console.log(err);
        }
    });

    Coupon.deleteMany({
        _id: { $in: couponObjectIds }
    }, function(err){
        if(err){
            console.log(err);
        }
    });
};

exports.getPriceForUser = function(req, res){
    let subscriptionStatus = {
        "DURATION_NONE": 0,
        "DURATION_ONE_MONTH": 1,
        "DURATION_THREE_MONTHS": 3,
        "COST_ONE_MONTH" : 1000,
        "COST_THREE_MONTHS" : 2000
    };
    try{
        User.find({
            _id: req.headers['user-id']
        }, 'couponId')
            .populate({
                path: 'couponId'
            })
            .exec(function(err, userData){
                if(err){
                    res.json(subscriptionStatus);
                }else{
                    try{
                        let subscriptionStatus = {
                            "DURATION_NONE": 0,
                            "DURATION_ONE_MONTH": 1,
                            "DURATION_THREE_MONTHS": 3,
                            "COST_ONE_MONTH": userData[0].couponId.oneMonth,
                            "COST_THREE_MONTHS": userData[0].couponId.threeMonths
                        };

                        res.json(subscriptionStatus);
                    }catch(e){
                        res.json(subscriptionStatus);
                    }
                }
            });
    }catch(e){
        console.log(e.message);
    }
};