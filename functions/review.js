const mongoose = require('mongoose');
const checkToken = require('../functions/checkToken');
const jobFunctionImported = require('../functions/job');

let ReviewParent = require('../models/reviewParent');
let ReviewTutor = require('../models/reviewTutor');

exports.getReviewsByUserIdForParent = function(req, res){
  if(checkToken.checkToken(req)){
      try{
          let userId = mongoose.Types.ObjectId(req.query.userId);

          ReviewParent
            .find({'userId': userId}, function(err, reviews){
                if(err){
                  console.log(err);
                  res.send(err);
                } else{
                  res.json(reviews);
                }
            });
      } catch(e){
          console.log(e.message);
          res.status(400).json({'Response': 'Oops something went wrong'});
      }
  } else{
      res.status(401).json({'Response': 'Invalid Token!'});
  }
};

exports.getReviewsByUserIdForTutor = function(req, res){
  if(checkToken.checkToken(req)){
      try{
          let userId = mongoose.Types.ObjectId(req.query.userId);

          ReviewTutor
            .find({'userId': userId}, function(err, reviews){
                if(err){
                  console.log(err);
                  res.send(err);
                } else{
                  res.json(reviews);
                }
            });
      } catch(e){
          console.log(e.message);
          res.status(400).json({'Response': 'Oops something went wrong'});
      }
  } else{
      res.status(401).json({'Response': 'Invalid Token!'});
  }
};

exports.reviewByParent = function(req, res){
  if(checkToken.checkToken(req))
  {
      let jobId = req.query.jobId;
      try{
          let r = req.body;

          let reviewTutor = new ReviewTutor({
              userId        : mongoose.Types.ObjectId(r.userId),
              reviewerName  : r.reviewerName,
              rating        : r.rating,
              reviewTags    : [],
              updatedAt     : new Date()
          });

          for(let i=0;i<r.reviewTags.length;i++){
              reviewTutor.reviewTags[i] = r.reviewTags[i];
          }

          reviewTutor.save(function(err, reviewData){
              if(err){
                console.log(err.message);
                res.send(err.message);
              } else{
                  jobFunctionImported.addReviewIdToJobForTutor(reviewData._id, jobId);
                  console.log("Review posted by parent successfully.");
                  res.status(200).json({'Response': 'Review posted by parent successfully.', '_id': reviewData._id });
              }
          })
      } catch(e){
          console.log(e.message);
          res.status(400).json({'Response': 'Oops something went wrong'});
      }
  }
  else{
      res.status(401).json({'Response': 'Invalid Token!'});
  }
};

exports.reviewByTutor = function(req, res){
    if(checkToken.checkToken(req))
    {
        let jobId = req.query.jobId;
        try{
            let r = req.body;

            let reviewParent = new ReviewParent({
                userId          : mongoose.Types.ObjectId(r.userId),
                reviewerName    : r.reviewerName,
                rating          : r.rating,
                reviewTags      : [],
                updatedAt       : new Date()
            });

            for(let i=0;i<r.reviewTags.length;i++){
                reviewParent.reviewTags[i] = r.reviewTags[i];
            }

            reviewParent.save(function(err, reviewData){
                if(err){
                  console.log(err.message);
                  res.send(err.message);
                } else{
                    jobFunctionImported.addReviewIdToJobForParent(reviewData._id, jobId);
                    console.log("Review posted by tutor successfully.");
                    res.status(200).json({'Response': 'Review posted by tutor successfully.', '_id': reviewData._id });
                }
            })
        } catch(e){
            console.log(e.message);
            res.status(400).json({'Response': 'Oops something went wrong'});
        }
    }
    else{
        res.status(401).json({'Response': 'Invalid Token!'});
    }
};

exports.updateReviewByParent = function(req, res){
  if(checkToken.checkToken(req)){
      let reviewId = req.query.reviewId;
      let reviewTutor = {
          userId        : mongoose.Types.ObjectId(req.body.userId),
          reviewerName  : req.body.reviewerName,
          rating        : req.body.rating,
          reviewTags    : [],
          updatedAt     : new Date()
      };
      for(let i=0;i<req.body.reviewTags.length;i++){
          reviewTutor.reviewTags[i] = req.body.reviewTags[i];
      }
    ReviewTutor
      .update(
        {'_id': mongoose.Types.ObjectId(reviewId)},
        {'$set': reviewTutor}, function(err){
          if(err){
            console.log(err);
            res.send(err);
          } else{
            res.status(200).json({'Response': 'Review updated successfully by parent'});
          }
        });
  } else{
    res.status(401).json({'Response': 'Invalid Token!'});
  }
};

exports.updateReviewByTutor = function(req, res){
  if(checkToken.checkToken(req)){
      let reviewId = req.query.reviewId;
      let reviewParent = {
          userId        : mongoose.Types.ObjectId(req.body.userId),
          reviewerName  : req.body.reviewerName,
          rating        : req.body.rating,
          reviewTags    : [],
          updatedAt     : new Date()
      };
      for(let i=0;i<req.body.reviewTags.length;i++){
          reviewParent.reviewTags[i] = req.body.reviewTags[i];
      }
    ReviewParent
      .update(
        {'_id': mongoose.Types.ObjectId(reviewId)},
        {'$set': reviewParent}, function(err){
          if(err){
            console.log(err);
            res.send(err);
          } else{
            res.status(200).json({'Response': 'Review updated successfully by tutor'});
          }
        });
  } else{
    res.status(401).json({'Response': 'Invalid Token!'});
  }
};