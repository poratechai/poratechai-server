let fs = require('fs');
let Promise = require('bluebird');
let checkToken = require('../functions/checkToken');
let constants = require('../functions/constants');

let fileName = './functions/matching/system_constants';

//JSON.parse is synchronous - so only JSON string is sent as the response instead of the JSON object
//The JSON string is parsed in front-end to reduce load on server
exports.readAndroidVersion = function(req, res){
    res.json(constants.androidVersionCode);
};

exports.readFile = function(req, res){
    // if(checkToken.checkToken(req)){
        let system_constants = new Promise(function(resolve, reject){
            fs.readFile(fileName, 'utf8', function(err, data){
                if(err){
                    console.log(err);
                    reject(err);
                }else{
                    // console.log(data);
                    resolve(data);
                }
            });
        });

        system_constants.then((data) => {
            res.json({message   : data});
        });
    // }else{
    //     res.send(401, "Unauthorized");
    // }
};

exports.readSystemWalletConstantsV1 = function(req, res){
    res.json(constants.walletConstants);
};

exports.readSystemWalletConstantsV2 = function(req, res){
    try{
        res.json(constants.subscriptionStatus);
    }catch(e){
        console.log(e);
    }
};

exports.readSupportNumber = function(req, res){
    res.json(constants.supportNumber);
};

exports.checkConnectivity = function(req, res){
    res.json("Connected to PorateChai Server Successfully");
};