const jwt = require('jsonwebtoken');
const config = require('../config/config.json');

const checkToken=(req)=> {

  const token = req.headers['x-access-token'];
  // const emailAddress = req.headers['email-address'];
  const userId = req.headers['user-id'];

  if (token) {

    try {
        //console.log("Tok: " + token);

        let decoded = jwt.verify(token, config.secret);

        return decoded.userId === userId;

    } catch(err) {
      console.log(err.message);
      return false;
    }

  } else {
    console.log("Invalid Token!");
    return false;
  }
};

exports.checkToken = checkToken;
