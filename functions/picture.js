const multer = require('multer');
const user = require('../models/user');
const mongoose = require('mongoose');
const cloudinary = require('cloudinary');
const constants = require('../functions/constants');

const upload = multer({
    dest:'images/',
    limits: {fileSize: 10000000, files: 1},
    fileFilter:  (req, file, callback) => {

        if (!file.originalname.match(/\.(jpg|jpeg)$/)) {
            return callback(new Error('Only Images are allowed !'), false)
        }

        callback(null, true);
    }
}).single('image')

const uploadProfilePicture = (req, res) => {
  upload(req, res, function(err){
  		if(err){
        res.status(400).json({message: err.message});
        return;
      };
      if (req.file == undefined) {
        res.status(500);
        return;
      }
  		// res.end("file uploaded")

      let userId = mongoose.Types.ObjectId(req.headers['user-id']);

      cloudinaryUpload(req.file.path)
        .then(function (result) {
          if(result){
            user
                .update(
                    { _id : userId},
                    { "$set" : {profilePicture: result.url}})
                .exec(function(err, editedUser)
                {
                    if(err) console.log(err);
                    else
                    {
                      if(editedUser){
                        setTimeout(function () {
                          res.status(200).send({message: 'Image Uploaded Successfully !', path: result.url});
                        }, 6000);
                      }
                      console.log(editedUser);
                    }
                });
          }
        })
        .catch(function (err) {
          res.status(200).send({message: 'Profile picture upload failed'});
        });
   });
};

const getProfilePicture =(req, res) => {
  let userId = mongoose.Types.ObjectId(req.query.userId);

  user
      .findOne(
        {_id: userId}
      )
      .exec(function (err, user) {
        if (!user) {
          res.status(500);
          return;
        }
        if(err)console.log(err);
        else{
          res.status(200).json({path: user.profilePicture});
        }
      });
};

const uploadCredentialPicture = (req, res) => {
    user.find({
        _id: mongoose.Types.ObjectId(req.headers['user-id'])
    }, 'credentialPictures'
    ).exec(function(err, data){
        if(err){
            console.log(err);
        }else{
            console.log(data);

            if(data[0].credentialPictures.length === 5 ){
                res.status(400).json({message: "Sorry, you can upload no more than 5 verification images"});
            }else{
                upload(req, res, function (err) {
                    if (err) {
                        res.status(400).json({message: err.message})
                    }
                    else {
                        let userId = req.headers['user-id'];

                        cloudinaryUpload(req.file.path)
                            .then(function (result) {
                                if(result){
                                    user
                                        .update(
                                            { _id : userId},
                                            { "$push" : {credentialPictures: result.url}})
                                        .exec(function(err, editedUser)
                                        {
                                            console.log(editedUser);
                                            if(err) console.log(err);
                                            else
                                            {
                                                setTimeout(function () {
                                                    res.status(200).json({message: 'Image Uploaded Successfully !', url: result.url});
                                                }, 6000);
                                            }
                                        });
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                            });
                    }
                });
            }
        }
    });
};

const getCredentialPictures =(req, res) => {
  let userId = mongoose.Types.ObjectId(req.headers['user-id']);

  user
      .findOne(
        {_id: userId}
      )
      .exec(function (err, user) {
        if (!user) {
          res.status(500);
          return;
        }
        if(err)console.log(err);
        else{
          res.status(200).json({paths: user.credentialPictures});
        }
      });
};

const cloudinaryUpload = (path) => {
  cloudinary.config({
    cloud_name: constants.cloudinary.CLOUD_NAME,
    api_key: constants.cloudinary.API_KEY,
    api_secret: constants.cloudinary.API_SECRET
  });

  return new Promise(function (resovle, reject) {
    cloudinary.uploader.upload(path, function (result) {
      if(result){
        resovle(result);
      }
      else {
        reject('Error');
      }
    });
  });
};

const deleteCredentialPicture = (req, res) => {
  let userId = mongoose.Types.ObjectId(req.headers['user-id']);
  let credentialPicture = req.query.path;

  let imageIdTokens         = credentialPicture.split("/");
  let imageIdWithExtension  = imageIdTokens[imageIdTokens.length-1];
  let imageId               = imageIdWithExtension.split('.')[0];

  cloudinaryDelete(imageId)
      .then((result)=>{
        if(result){
            user
                .findOneAndUpdate({
                        _id: userId
                    },{
                        $pull: {credentialPictures: credentialPicture},
                        $set: {isVerifiedByVerificationImages: 'false'}
                    }
                )
                .exec(function (err, updatedUser) {
                    if(err){
                        console.log(err);

                        res.status(500).json({message: "Failed to delete image"});
                    }
                    else {
                        if(updatedUser){
                            res.status(200).json({message: 'Image deleted successfully', url: credentialPicture});
                        }
                    }
                });
        }
    })
      .catch((err)=>{
          console.log(err);

          res.status(500).json({message: "Failed to delete image"});
      });
};

const cloudinaryDelete = (url) =>{
    cloudinary.config({
        cloud_name: constants.cloudinary.CLOUD_NAME,
        api_key: constants.cloudinary.API_KEY,
        api_secret: constants.cloudinary.API_SECRET
    });

    return new Promise(function (resovle, reject) {
        cloudinary.uploader.destroy(url, function (result) {
            if(result){
                resovle(result);
            }
            else {
                reject('Error');
            }
        });
    });
};

exports.uploadProfilePicture = uploadProfilePicture;
exports.getProfilePicture = getProfilePicture;
exports.uploadCredentialPicture = uploadCredentialPicture;
exports.getCredentialPictures = getCredentialPictures;
exports.deleteCredentialPicture = deleteCredentialPicture;
