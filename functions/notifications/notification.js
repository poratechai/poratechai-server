var admin = require("firebase-admin");
var serviceAccount = require("./poratechai-43979-firebase-adminsdk-3tksl-c223dba4e8.json");
var FCM = require('fcm-push');
var serverkey = 'AAAAdndmOWE:APA91bHXznA63bLSmmlJIKHQv4uNC4o2qLljYVjgWBVx0IxvNRMscnWoQuqOldpigRwFm6Nn6OenlUVs5dxt1jj0lfUJz_MEeUlKwuOS4GLbNJrpP5hDoDEJx2EUWHfi6-e8IAef7HPB';
var fcm = new FCM(serverkey);
const User = require('../../models/user');
const Const = require('../constants');

/*
    OPERATION TYPES
    1 - ADD
    2 - REMOVE
    3 - UPDATE

    FEED TYPES
    1 - POSTED JOBS
    2 - ACTIVE POSTED JOBS
    3 - POSTED JOB HISTORY
    4 - MATCHES
    5 - ACTIVE JOBS
    6 - JOB HISTORY
*/

//from tutor to parent
exports.tutorSendsContractRequestToParent = function(jobData){
    console.log(jobData);
    let message = {
        to: jobData.deviceIdToken,
        data: {
            title: "New Contract Request",
            body: "Tutor " + jobData.tutorFullName + " has sent a new contract request for job " + jobData.jobTitle,
            fragmentType : Const.notificationFragmentType.JOB_FEED,
            operationType : Const.notificationOperationType.UPDATE,
            feedType: Const.notificationFeedType.POSTED_JOBS,
            jobIdOrMatchId: jobData.jobId
        }
    };
    sendNotification(message);
};

//from parent to tutor
exports.parentOrStudentViewingContractRequest = function(jobData){
    console.log(jobData);
    let message = {
        to: jobData.deviceIdToken,
        data: {
            title: "Contract Request Accepted",
            body: "Parent/student is reviewing your contract request for job " + jobData.jobTitle,
            fragmentType : Const.notificationFragmentType.JOB_FEED,
            operationType: Const.notificationOperationType.UPDATE,
            feedType: Const.notificationFeedType.MATCHES,
            jobIdOrMatchId: jobData.matchId
        }
    };
    sendNotification(message);
};
exports.tutorAcceptedForInspectionPhase = function(jobData){
    console.log(jobData);
    let message = {
        to: jobData.deviceIdToken,
        data: {
            title: "Accepted!",
            body: "Parent/student has accepted you for Inspection Phase for job " + jobData.jobTitle,
            fragmentType : Const.notificationFragmentType.JOB_FEED,
            operationType: Const.notificationOperationType.UPDATE,
            feedType: Const.notificationFeedType.MATCHES,
            jobIdOrMatchId: jobData.matchId
        }
    };
    sendNotification(message);
};

//from server to parent
exports.notificationAfter14DaysOfInspectionToParent = function(jobData){
    console.log("Notification: " + jobData);

    let messageToParent1 = {
        to: jobData.deviceIdToken,
        data: {
            title: "Active Phase",
            body: "Job " + jobData.jobTitle + " in Active Phase after 14 days",
            fragmentType : Const.notificationFragmentType.JOB_FEED,
            operationType: Const.notificationOperationType.REMOVE,
            feedType: Const.notificationFeedType.POSTED_JOBS,
            jobIdOrMatchId: jobData.jobId
        }
    }
    sendNotification(messageToParent1);
    let messageToParent2 = {
        to: jobData.deviceIdToken,
        data: {
            title: "Active Phase",
            body: "Job " + jobData.jobTitle + " in Active Phase after 14 days",
            fragmentType : Const.notificationFragmentType.JOB_FEED,
            operationType: Const.notificationOperationType.ADD,
            feedType: Const.notificationFeedType.ACTIVE_POSTED_JOBS,
            jobIdOrMatchId: jobData.jobId
        }
    }
    sendNotification(messageToParent2);
};

//from server to tutor
exports.notifyTutorAboutNewMatch = function(jobData){
    console.log(jobData);

    let message = {
        to: jobData.deviceIdToken,
        data: {
            title: "New Match!",
            body: jobData.jobTitle,
            fragmentType : Const.notificationFragmentType.JOB_FEED,
            operationType: Const.notificationOperationType.ADD,
            feedType: Const.notificationFeedType.MATCHES,
            jobIdOrMatchId: jobData.matchId
        }
    };
    sendNotification(message);
};
exports.notificationAfter14DaysOfInspectionToTutor = function(jobData){
    console.log("Notification: " + jobData);

    let messageToTutor1 = {
        to: jobData.deviceIdToken,
        data: {
            title: "Active Phase",
            body: "Job " + jobData.jobTitle + " in Active Phase after 14 days",
            fragmentType : Const.notificationFragmentType.JOB_FEED,
            operationType: Const.notificationOperationType.REMOVE,
            feedType: Const.notificationFeedType.MATCHES,
            jobIdOrMatchId: jobData.matchId
        }
    };
    sendNotification(messageToTutor1);
    let messageToTutor2 = {
        to: jobData.deviceIdToken,
        data: {
            title: "Active Phase",
            body: "Job " + jobData.jobTitle + " in Active Phase after 14 days",
            fragmentType : Const.notificationFragmentType.JOB_FEED,
            operationType: Const.notificationOperationType.ADD,
            feedType: Const.notificationFeedType.ACTIVE_JOBS,
            jobIdOrMatchId: jobData.jobId
        }
    }
    sendNotification(messageToTutor2);
};

//from server to user
exports.notifyAboutVerificationStatus = function(userData){
    let messageToUser = {
        to: userData.deviceIdToken,
        data: {
            title: "Porate Chai Verification",
            body: "Congratulations! You are verified. You can now send contract requests!",
            fragmentType : Const.notificationFragmentType.PROFILE,
            feedType : Const.notificationFeedType.PROFILE
        }
    };
    return sendNotification(messageToUser);
};

sendNotification = function(message){
    let promiseToSendNotification = new Promise(function(resolve, reject){
        if(message.to!=="" && message.to!=null){
            fcm.send(message, function(err, res){
                if(err){
                    console.log("Something has gone wrong!");
                    console.log(err);

                    reject("Notification error: " + err);
                } else {
                    console.log("Successful sent with response: ", res);

                    resolve("Notification successfully sent");
                }
            });
        }else{
            console.log("Device Id Token null - Notification not sent");

            reject("Notification error - Device Id Token null");
        }
    });

    return promiseToSendNotification;
};