const Match = require('../models/match');
const profile = require('../functions/profile');
const Job = require('../models/job');
const User = require('../models/user');
const Wallet = require('../models/wallet');
const Notification = require('./notifications/notification');
const constants = require('../functions/constants');
const moment = require('moment');
const Promise = require('bluebird');
const mongoose = require('mongoose');
const walletFunctionImported = require('../functions/wallet');
mongoose.Promise = Promise;

const currentContractFunction = require('../functions/contract');

//Phases: 0 - Match Phase
//        1 - Contract Review Request Sent Phase
//        2 - Contract Review Request Accepted Phase
//        3 - Inspection Phase
//        4 - Active Phase
//        5 - Archive Phase
////////////////////////////////
//sendContractReviewRequest : 0->1
//acceptContractReviewRequest: 1->2
//acceptTutor: 2->3
//cancelJobinPhase0: 0 -> -1 Match collection
//cancelJobinPhase1: 1 -> -1 Match collection
//cancelJobinPhase2: 2 -> -1 Match collection
//cancelJobinPhase3: 3 -> -1 Match collection
//cancelJobinPhase4: 4 ->  5 Job collection
//parentCancelsJob : Job deleted from Match, Job and User collection


//Ratul - start
exports.sendContractReviewRequestV1 = function(req, res)
{
    let matchId = req.query.matchId;

    console.log(matchId + " " + req.body.daysPerWeek + " " + req.body.salaryOffer + " " + req.body.message);

    let jobData = {
        deviceIdToken: "",
        tutorFullName: "",
        jobTitle: "",
        jobId: ""
    };

    Match.findOne({_id: matchId})
        .populate({
            path: 'tutorProfile',
            populate: {
                path: 'walletId'
            }
        })
        .populate({
            path: 'jobData',
            populate: {
                path: 'userProfile'
            }                          
        })
        .exec(function(err, data){
                if(err){
                    console.log(err);
                } else{
                    return Promise.resolve(data);
                }
            }
        ).then((data) => {
            if(data.tutorProfile[0].walletId.contractBalance >= ((data.jobData[0].salary/constants.walletConstants.UNIT_SALARY_STEP)|0)){
                return Promise.resolve(data);
            }else{
                return Promise.reject(data);
            }
        }).then((resolvedData) => {
            changeStatus(matchId, 0, 1);

            let resolvedDataPromise = Promise.resolve(resolvedData);
            let matchUpdate = Match.update(
                { "_id" : matchId },
                { "$set": {
                        "daysPerWeek" : req.body.daysPerWeek,
                        "salaryOffer" : req.body.salaryOffer,
                        "message"     : req.body.message
                    }
                }).exec(function(err, updateInfo){
                    if(err){
                        console.log("Contract details posting was not successful" + err);
                    }else{
                        console.log("Contract details posted successfully");
                    }
                });
            return Promise.all([resolvedDataPromise, matchUpdate]);
        }, (rejectedData) => {
            res.json({message:"You don't have enough tokens to send this contract request"});

            throw new Error("Not enough tokens");
        }).then(([data, updateInfo]) => {
            let resolvedData = Promise.resolve(data);
            let walletUpdate = Wallet.update(
                { "userId" : data.tutorId },
                { "$set": {
                        contractBalance : data.tutorProfile[0].walletId.contractBalance - ((data.jobData[0].salary/constants.walletConstants.UNIT_SALARY_STEP)|0)
                    }
                }).exec(function(err){
                    if(err){
                        console.log("Balance reduction was not successful" + err);
                    }else{
                        console.log("Balance reduced for contract");

                        return Promise.resolve(data);
                    }
                });
            return Promise.all([resolvedData, walletUpdate]);
        }).then(([data, updateInfo]) => {
            console.log(data);
            jobData.deviceIdToken = data.jobData[0].userProfile[0].deviceIdToken;
            jobData.tutorFullName = data.tutorProfile[0].fullName;
            jobData.jobTitle = data.jobData[0].jobTitle;
            jobData.jobId = data.jobId;

            Notification.tutorSendsContractRequestToParent(jobData);

            res.status(200).json({message:"Contract Request Sent Successfully!"});
        }).catch((e) => {
            console.log(e);
        });
};

exports.sendContractReviewRequestV2 = function(req, res)
{
    let matchId = req.query.matchId;

    console.log(matchId + " " + req.body.daysPerWeek + " " + req.body.salaryOffer + " " + req.body.message);

    let jobData = {
        deviceIdToken: "",
        tutorFullName: "",
        jobTitle: "",
        jobId: ""
    };

    Match.findOne({_id: matchId})
        .populate({
            path: 'tutorProfile',
            populate: {
                path: 'walletId'
            }
        })
        .populate({
            path: 'jobData',
            populate: {
                path: 'userProfile'
            }
        })
        .exec()
        .then((matchData) => {
            return checkVerificationStatus(matchData);
        })
        .then(
            (resolvedMatchData) => {  //tutor is verified by verification images
                return currentContractFunction.checkSubscriptionValidityForContractRequest(resolvedMatchData);  //check subscription
        },
            (rejectedMatchData) => {  //tutor is not verified by verification images
                res.status(403).json({message:"Upload verification images and get verified before sending contract request"});

                throw new Error("Not verified");
        }).then(
            (resolvedMatchData) => {  //tutor's subscription is valid
                changeStatus(matchId, 0, 1);

                let resolvedDataPromise = Promise.resolve(resolvedMatchData);
                let matchUpdate = Match.update(
                    { "_id" : matchId },
                    { "$set": {
                            "daysPerWeek" : req.body.daysPerWeek,
                            "salaryOffer" : req.body.salaryOffer,
                            "message"     : req.body.message
                        }
                    }).exec(function(err, updateInfo){
                    if(err){
                        console.log("Contract details posting was not successful" + err);
                    }else{
                        console.log("Contract details posted successfully");
                    }
                });
                return Promise.all([resolvedDataPromise, matchUpdate]);
        },
            (rejectedMatchData) => {  //tutor's subscription is invalid
                if(rejectedMatchData instanceof Error) throw Error(rejectedMatchData.message); //Error propagated from "Not verified"

                res.status(402).json({message:"Subscribe to send contract request"});

                throw new Error("No subscription");
        }).then(([data, updateInfo]) => {
            jobData.deviceIdToken = data.jobData[0].userProfile[0].deviceIdToken;
            jobData.tutorFullName = data.tutorProfile[0].fullName;
            jobData.jobTitle = data.jobData[0].jobTitle;
            jobData.jobId = data.jobId;

            Notification.tutorSendsContractRequestToParent(jobData);

            res.status(200).json({message:"Contract Request Sent Successfully!"});
        }).catch((e) => {
            console.error("Sending contract request error:\n" + e.stack);
        });
};

exports.acceptContractReviewRequest = function(req, res)
{
    let matchId = req.query.matchId;
    console.log(matchId);

    changeStatus(matchId, 1, 2);

    let jobData = {
        matchId: "",
        deviceIdToken: "",
        jobTitle: ""
    }
    Match.findOne({_id: matchId})
        .populate({
            path: 'jobData' 
        })
        .populate({
            path: 'tutorProfile'
        })
        .exec(
            function(err, data){
                if(err){
                    console.log(err);
                } else {
                    console.log("ashcheh");
                    jobData.deviceIdToken = data.tutorProfile[0].deviceIdToken;
                    jobData.matchId = data._id;
                    jobData.jobTitle = data.jobData[0].jobTitle;

                    Notification.parentOrStudentViewingContractRequest(jobData);
                    res.status(200).json({message:"Contract Request Accepted"});
                }
            }
        )
}

const changeStatus = function(mId, previousStatus, currentStatus)
{
    try
    {
        Match.update({
                _id : mId
            },
            {
                previousStatus : previousStatus,
                currentStatus : currentStatus,
                dateOfStatusChange : new Date()
            },
            function(err)
            {
                if(err){
                    console.log(err);
                }
                else{
                    console.log("Contract status updated - " + mId);
                }
            });
    }
    catch(e)
    {
        console.log(e);
    }
}

const checkVerificationStatus = function(matchData)
{
    let promiseToCheckVerificationStatus = new Promise(function(resolve, reject){
        User.findOne({
            _id: mongoose.Types.ObjectId(matchData.tutorId)
        })
            .exec()
            .then((data) => {
                if(data.isVerifiedByVerificationImages === 'true'){
                    resolve(matchData);
                }else if(data.isVerifiedByVerificationImages === 'false'){
                    reject(matchData);
                }
            });
    });

    return promiseToCheckVerificationStatus;
};

exports.checkSubscriptionValidityForContractRequest = function(matchData){
    let promiseToCheckAndUpdateSubscription = walletFunctionImported.checkSubscriptionValidity(matchData.tutorProfile[0].walletId);
    let promiseToResolveSubscriptionWithMatchData = new Promise(function(resolve, reject){
        promiseToCheckAndUpdateSubscription.then((resolvedData)=>{  //After checking subscription, subscriptionMonths is updated (subscription update resolved)
            console.log("Subscription invalid");
            reject(matchData);
        },(rejectedData)=>{     //After checking subscription, subscription is valid (subscription update rejected)
            console.log("Subscription valid");
            resolve(matchData);
        }).catch((e)=>{
            console.log(e);
        });
    });

    return promiseToResolveSubscriptionWithMatchData;
};
//Ratul - end

//Tanveer - start

//Accept Tutor
exports.acceptTutor = function(req, res)
{
    let matchId = req.query.matchId;
    console.log(matchId);

    changeStatus(matchId, 2, 3);

    let jobData = {
        deviceIdToken: '',
        tutorId: '',
        jobTitle: '',
        matchId: ''
    };

    Match.findOne({_id: matchId})
        .populate({
            path: 'jobData'
        })
        .populate({
            path: 'tutorProfile'
        })
        .exec(
            function(err, data){
                jobData.deviceIdToken = data.tutorProfile[0].deviceIdToken;
                jobData.tutorId = data.tutorId;
                jobData.jobTitle = data.jobData[0].jobTitle;
                jobData.matchId = data._id;

                Notification.tutorAcceptedForInspectionPhase(jobData);
            }
        );
    res.status(200).json({"Response:": "Tutor has been accepted"});
};

//Cancel functions
exports.cancelJobinPhase1 = function(req, res)
{//tutor or parent cancels a contract review request before parent accepts the request
    let matchId = req.query.matchId;
    console.log(matchId);

    changeStatus(matchId, 1, -1);
    res.status(200).json({"Response:": "Contract review request canceled by tutor while job in Phase 1."});
};

exports.cancelJobinPhase2 = function(req, res)
{//tutor or parent cancels a contract review if either of them  cancels after the parent accepts the request
    let matchId = req.query.matchId;
    console.log(matchId);

    changeStatus(matchId, 2, -1);
    res.status(200).json({"Response:": "Contract review request canceled while job in Phase 2."});
};

exports.cancelJobinPhase3 = function(req, res)
{//tutor or parent cancels a job in inspection phase
    let matchId = req.query.matchId;
    console.log(matchId);

    changeStatus(matchId, 3, -1);
    res.status(200).json({"Response:": "Job canceled in Inspection phase."});
};

exports.cancelJobinPhase4 = function(req, res)
{//tutor or parent cancels a job in active phase,
    let jobId = req.query.jobId;
    console.log(jobId);

    Job.update(
        {'_id': jobId},
        {'status': 5}, function(err){
            if(err){
                console.log(err);
            } else {
                console.log('status changed in job');
            }
        }
    );
    res.status(200).json({"Response:": "Job canceled (archived) in Active phase."});
};

exports.cancelJobinPhase0 = function(req, res)
{//tutor cancels a job in match phase
    let matchId = req.query.matchId;
    console.log(matchId);

    changeStatus(matchId, 0, -1);
    res.status(200).json({"Response:": "Job canceled by tutor in Match phase."});
};

exports.parentCancelsJob = function(req, res)
{//parent cancels a job which has status 0
    let jobId = req.query.jobId;
    console.log("jobId: " + jobId);

    Job.remove({'_id': jobId}, function(err){
        if(err){
            console.log(err);
        } else {
            console.log("Deleted from Job model");
        }
    });

    User.update(
        {'postedJobData.jobId': jobId},
        {$pull: {'postedJobData': {'jobId': jobId}}},
        function(err){
            if(err){
                console.log(err);
            } else {
                console.log('Job Id deleted from User model');
            }
        }
    );
    Match.remove({'jobId': jobId}, function(err){
        if(err){
            console.log(err);
        } else {
            console.log("Deleted from Match model");
        }
    });

    res.status(200).json({"Response:": "Job canceled by parent, and deleted from User and Job Model."});
};

//Tanveer - end
