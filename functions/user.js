let mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
let User = require('../models/user');
let matchingClient = require('./matching/matching_client');
let checkToken = require('../functions/checkToken');
let notification = require('./notifications/notification');

//For Jobs section - start

exports.addJobToUser = function(userId, jobId) {
    try
    {
        User.update(
            {
                _id : userId
            },
            { "$push" : { "postedJobData" : jobId }}
        ).exec(function(err, val)
        {
            if(err) console.log(err.message);
            else console.log(val);
        });
    }
    catch(e)
    {
        console.log("Exception - Adding Job to User " + e.stack);
    }
};

//For Jobs section - end

exports.updateInstitutions = function(req, res){
    try{
        if(checkToken.checkToken(req)){
            User.findOne({
                _id: mongoose.Types.ObjectId(req.headers['user-id'])
            },function(err, data){
                if(err){
                    console.log(err);
                }else{
                    data.institution.set(0, req.body.school);
                    data.institution.set(1, req.body.college);
                    data.institution.set(2, req.body.university);

                    data.markModified("institution");

                    data.save(function(err){
                        if(err){
                            res.json({message: "Update unsuccessful"});
                        }else{
                            res.json({message: "Institutions updated successfully"});
                        }
                    });
                }
            })
        }else{
            res.status(401).json({message: "Unauthorized"});
        }
    }catch(e){
        console.log(e);
    }
};

exports.updateVerificationImages = function(req, res){
    try{

    }catch(e){
        console.log(e);
    }
};

//Matching to be called - start

exports.updateSkills = function(req, res){
    if(checkToken.checkToken(req)){
        try{
            User.findOne({
                _id: mongoose.Types.ObjectId(req.headers['user-id'])
            },function(err, data){
                if(err){
                    console.log(err);
                }else{
                    let arrayLimit = 0;

                    data.preferredSubcategory = [];

                    if(req.body.skills != null){
                        for(i=0;i<req.body.skills.length;i++){
                            data.preferredSubcategory.set(i, req.body.skills[i]);
                        }
                    }

                    data.save(function(err){
                        if(err){
                            res.json({message: "Update unsuccessful"});
                        }else{
                            checkTutorUpdateFlag(data)
                                .then(function(){
                                    res.json({message: "Skills updated successfully"});
                                })
                                .catch(function(e){
                                    console.log(e);
                                });
                        }
                    })
                }
            });
        }catch(e){
            console.log(e);
        }
    }else{
        res.status(401).json({message: "Unauthorized"});
    }
};

exports.updateClasses = function(req, res){
    if(checkToken.checkToken(req)){
        try{
            User.findOne({
                _id: mongoose.Types.ObjectId(req.headers['user-id'])
            },function(err, data){
                if(err){
                    console.log(err);
                }else{
                    data.preferredClassToTeach = [];

                    if(req.body.classes != null){
                        for(i=0;i<req.body.classes.length;i++){
                            data.preferredClassToTeach.set(i, req.body.classes[i]);
                        }
                    }

                    data.save(function(err){
                        if(err){
                            res.json({message: "Update unsuccessful"});
                        }else{
                            checkTutorUpdateFlag(data)
                                .then(function(){
                                    res.json({message: "Class preference updated successfully"});
                                })
                                .catch(function(e){
                                    console.log(e);
                                });
                        }
                    })
                }
            });
        }catch(e){
            console.log(e);
        }
    }else{
        res.status(401).json({message: "Unauthorized"});
    }
};

exports.updateSubjects = function(req, res){
    if(checkToken.checkToken(req)){
        try{
            User.findOne({
                _id: mongoose.Types.ObjectId(req.headers['user-id'])
            },function(err, data){
                if(err){
                    console.log(err);
                }else{
                    data.preferredSubjects = [];

                    if(req.body.subjects != null){
                        for(i=0;i<req.body.subjects.length;i++){
                            data.preferredSubjects.set(i, req.body.subjects[i]);
                        }
                    }

                    data.save(function(err){
                        if(err){
                            res.json({message: "Update unsuccessful"});
                        }else{
                            checkTutorUpdateFlag(data)
                                .then(function(){
                                    res.json({message: "Subject preference updated successfully"});
                                })
                                .catch(function(e){
                                    console.log(e);
                                });
                        }
                    })
                }
            });
        }catch(e){
            console.log(e);
        }
    }else{
        res.status(401).json({message: "Unauthorized"});
    }
};

exports.updateLocation = function(req, res){
    if(checkToken.checkToken(req)){
        try{
            User.findOneAndUpdate({
                _id: mongoose.Types.ObjectId(req.headers['user-id'])
            },{
                $set: {
                    locationLat : req.body.locationLat,
                    locationLong: req.body.locationLong,
                    radius      : req.body.radius,
                    location    : req.body.location
                }
            }).exec(function(err, data){
                if(err){
                    res.json({message: "Update unsuccessful"});
                }else{
                    checkTutorUpdateFlag(data)
                        .then(function(){
                            res.json({message: "Location updated successfully"});
                        })
                        .catch(function(e){
                            console.log(e);
                        });
                }
            })
        }catch(e){

        }
    }else{
        res.status(401).json({message: "Unauthorized"});
    }
};

const checkTutorUpdateFlag = function(data){
    let promiseToCheckTutorUpdateFlag = Promise.resolve();

    if(data.preferredSubcategory!=null && data.preferredSubcategory.length>0 &&
        data.preferredClassToTeach!=null && data.preferredClassToTeach.length>0 &&
        data.preferredSubjects!=null && data.preferredSubjects.length>0 &&
        data.locationLat!=null && !Number.isNaN(data.locationLat) &&
        data.locationLong!=null && !Number.isNaN(data.locationLong) &&
        data.radius!=null && !Number.isNaN(data.radius)){

        promiseToCheckTutorUpdateFlag = User.update({
            _id: mongoose.Types.ObjectId(data._id)
        },{
            $set: {
                "tutorUpdateFlag" : 'true'
            }
        }).exec().then(function(){
            try{
                matchingClient.sendSignal('run 1 matching');
            }catch(e){
                console.log(e);
            }
        });
    }else{
        promiseToCheckTutorUpdateFlag = User.update({
            _id: mongoose.Types.ObjectId(data._id)
        },{
            $set: {
                "tutorUpdateFlag" : 'false'
            }
        }).exec();
    }

    return promiseToCheckTutorUpdateFlag;
};

//Matching to be called - end

exports.updateDaysPerWeek = function(req, res){
    if(checkToken.checkToken(req)){
        try{
            User.update({
                _id: mongoose.Types.ObjectId(req.headers['user-id'])
            }, {
                $set: { preferredDaysPerWeek: req.body.daysPerWeek }
            }).exec(function(err){
                if(err){
                    res.json({message: "Update unsuccessful"});
                }else{
                    res.json({message: "Days Per Week updated successfully"});
                }
            })
        }catch(e){
            console.log(e);
        }
    }else{
        res.status(401).json({message: "Unauthorized"});
    }
};

exports.updateSalary = function(req, res){
    if(checkToken.checkToken(req)){
        try{
            User.update({
                _id: mongoose.Types.ObjectId(req.headers['user-id'])
            }, {
                $set: { minPreferredSalary: req.body.salary }
            }).exec(function(err){
                if(err){
                    res.json({message: "Update unsuccessful"});
                }else{
                    res.json({message: "Salary updated successfully"});
                }
            })
        }catch(e){
            console.log(e);
        }
    }else{
        res.status(401).json({message: "Unauthorized"});
    }
};

//Verification from Admin app - start
exports.updateVerificationFromAdminApp = function(req, res){
    console.log(req.body.email);

    User.findOneAndUpdate({
        email: req.body.email
    }, {
        "$set": { isVerifiedByVerificationImages: 'true' }
    }, {
        projection: { "deviceIdToken": 1 }
    })
        .exec()
        .then((verifiedUser) => {
            return notification.notifyAboutVerificationStatus(verifiedUser);       //returns Promise
    })
        .then((message) => {
            res.status(200).json(req.body.email + " Verified! " + message);
        }, (message) => {
            res.status(503).json(req.body.email + " Verified! " + message);
        })
        .catch((e) => {
            console.log(e);
            res.status(503).json("Verification error!");
    });
};
//Verification from Admin app - end