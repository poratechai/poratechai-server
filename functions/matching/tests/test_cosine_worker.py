import unittest
import sys
import numpy as np
sys.path.insert(0, '../')

from cosine_worker import cos_sim, cos_sim_mask

class TestCosSim(unittest.TestCase):

    def test_cos_sim_for_0(self):
        a = np.array([
            [1,0,0],
            [0,1,0],
            [0,0,1],
            [1,1,0],
            [1,0,1],
            [0,1,1]
            ])
        b = np.array([
            [0,1,1],
            [1,0,1],
            [1,1,0],
            [0,0,1],
            [0,1,0],
            [1,0,0]
            ])
        b = b.transpose()
        result = cos_sim(a,b)
        for i in range(6):
            self.assertEqual(result[i][i], [0])
        #self.assertEqual(result, [0])
    
    def test_cos_sim_for_1(self):
        a = np.array([
            [1,0,0],
            [0,1,0],
            [0,0,1]
        ])
        b = a.copy().transpose()
        result = cos_sim(a,b)
        for i in range(3):
            self.assertEqual(result[i][i], [1])

    def test_cos_sim_mask_for_0(self):
        a = np.array([
            [0, 0, 0],
            [1, 0, 0],
            [0.5, 0, 0],
            [0.5, 0.5, 0],
            [0.5, 0.5, 0.5],
            [0.6, 0.5, 0],
            [0.6, 0.5, 0.4],
            [1, 1, 1]
        ])
        result = cos_sim_mask(a)
        self.assertTrue(np.array_equal(result, 
            [
                [0, 0, 0],
                [1, 0, 0],
                [1, 0, 0],
                [1, 1, 0],
                [1, 1, 1],
                [1, 1, 0],
                [1, 1, 0],
                [1, 1, 1]
            ]
        ))
