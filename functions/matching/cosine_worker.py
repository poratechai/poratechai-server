import numpy as np

def cos_sim(a, b):
    g = np.dot(a,b)
    #print(g)
    norm_a = np.apply_along_axis(np.linalg.norm, 1, a)
    norm_b = np.apply_along_axis(np.linalg.norm, 0, b)
    norm_matrix = norm_a.reshape(a.shape[0],1)*norm_b.reshape(1,b.shape[1])
    cosine_similarities = g / norm_matrix
    # print(cosine_similarities)
    # print('\n')
    return cosine_similarities

def cos_sim_mask(cosine_similarities, filter):
    #row_medians = np.apply_along_axis(np.median, 1, cosine_similarities)
    row_medians = np.percentile(cosine_similarities, 50, axis=1)
    row_medians = row_medians.reshape(row_medians.shape[0],1)
    # print('row_medians', row_medians)
    # print('cosine_similarities', cosine_similarities)
    #mask = cosine_similarities != 0 and cosine_similarities>=row_medians
    #mask = np.all([(cosine_similarities != 0), (cosine_similarities >= row_medians)], axis=0)
    if(filter == 'general'):
        if(cosine_similarities.shape[1] == 1):
          mask = np.array([[True]])
        elif(cosine_similarities.shape[1] < 5):
            mask = cosine_similarities > 0.99999
        else:
            mask = np.logical_and(
                cosine_similarities != 0, 
                cosine_similarities >= row_medians)
    elif(filter == 'subcats'):
        # if num(preferred subcats) == 4, cos_sim = 0.5; if n == 3, c_s = 0.577; if n == 2, c_s = 0.707
        # implication - tutors with narrower array of specialties will be preferred more
        mask = cosine_similarities >= 0.3
    # print('mask', mask)
    cosine_similarities[mask] = 1
    cosine_similarities[~mask] = 0
    return cosine_similarities

def cos_sim_bin(a, b, c):
    cosine_similarities = cos_sim(a, b)
    # print('cosine_similarities: ' + str(cosine_similarities))
    cosine_similarities = cos_sim_mask(cosine_similarities, c)
    #print(cosine_similarities)
    # print(c)
    
    return cosine_similarities