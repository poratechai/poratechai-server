# import socket programming library
import socket

# import thread module
from _thread import start_new_thread
import threading
import time

# import system modules
import os
import sys
import subprocess

n = 0
count = 0
client = 0
print_lock = threading.Lock()
WINDOW_SIZE = 5

# thread function
def connection(client):
    while True:
        # data received from client
        data = client.recv(1024)
        global n
        global count
        if not data:
            print('Bye')
            break
        try:
            print(data)
            message = str(data.decode('ascii'))
            if n <= WINDOW_SIZE and message == 'run 1 matching':
                print('ACK - stashed match call')
                count = count + 1
                response = 'ACK - stashed match call'

                # send reponse to client
                client.send(response.encode('ascii'))
        except:
            print('ERROR matching: ', sys.exc_info()[0], sys.exc_info()[1])
            pass

def timer():
    global n
    global count
    global client
    while True:
        n = n + 1
        time.sleep(1)
        if(n == WINDOW_SIZE):
            n = 0
            if(count > 0):
                response = 'ACK - finished running matching routine'
                print('RUNNING - matching routine')
                count = 0
                print(os.getcwd())
                os.chdir('./functions/matching')
                subprocess.call(['python', 'mongopython.py'])
                os.chdir('../..')
                client.send(response.encode('ascii'))

def Main():
    global client
    host = ''

    # reverse a port on your computer
    # in our case it is 12345 but it
    # can be anything
    port = 12345
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    print('matching server socket binded to port', port)

    # put the socket into listening mode
    s.listen(2)
    print('matching server socket is listening...')

    # start timer
    start_new_thread(timer, ())

    # a forever loop until client wants to exit
    while True:

        # establish connection with client
        client, addr = s.accept()
        print('Connected to :', addr[0], ':', addr[1])

        # Start a new thread and return its identifier
        start_new_thread(connection, (client,))

if __name__ == '__main__':
    Main()