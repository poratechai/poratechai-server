import pandas as pd

def get_jobs(dict_list_raw_jobs, subject_keys, class_to_teach_keys):
    subjects_list = [
        list(val['subjects'])
        for val in dict_list_raw_jobs
    ]

    class_to_teach = [
        val['class']
        for val in dict_list_raw_jobs
    ]

    dict_list_jobs = []

    for i in range(len(dict_list_raw_jobs)):
        dict_list_jobs.append(dict.fromkeys(subject_keys + class_to_teach_keys, 0))

    for i in range(len(dict_list_raw_jobs)):
        dict_list_jobs[i]['Salary']=dict_list_raw_jobs[i]['salary']
        dict_list_jobs[i]['Gender Preference'] = dict_list_raw_jobs[i]['tutorGenderPreference']
        if not class_to_teach[i]=='':
            dict_list_jobs[i][class_to_teach[i]]=1

        for chunk in subjects_list[i]:
            if chunk in dict_list_jobs[i]:
                dict_list_jobs[i][chunk]=1

    df = pd.DataFrame(dict_list_jobs)
    jobs = df.as_matrix()
    jobs[jobs=='m'] = 2
    jobs[jobs=='f'] = 1
    jobs[jobs=='any'] = 1
    return jobs

def get_job_subcats(dict_list_raw_jobs, subcategory_keys):
    subcategory = [
        val['subcategory']
        for val in dict_list_raw_jobs
    ]    

    dict_list_jobs = []

    for i in range(len(dict_list_raw_jobs)):
        dict_list_jobs.append(dict.fromkeys(subcategory_keys, 0))

    for i in range(len(dict_list_raw_jobs)):
        
        dict_list_jobs[i][subcategory[i]]=1
        #print(dict_list_jobs[i])
        
    #print(dict_list_jobs)
    df = pd.DataFrame(dict_list_jobs)
    job_subcats = df.as_matrix()
    
    return job_subcats