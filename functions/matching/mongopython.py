import pandas as pd
import numpy as np
import bson
from pymongo import MongoClient
import datetime
import json
import sys

from cosine_worker import cos_sim_bin
from haversine_worker import haversine_np
from job_data import get_jobs, get_job_subcats
from tutor_data import get_tutors, get_tutor_subcats

## CONSTANTS
RANGE_LIMIT = 10 #km

## Database connection and program init
client = MongoClient('mongodb://hyptastic001:the3men@ds229448.mlab.com:29448/projectx')
#client = MongoClient('mongodb://localhost:27017/projectx')
db = client.projectx

with open('./system_constants') as f:
    data = json.load(f)

classes_to_teach = data["classesToTeach"]

subcategories = []
for key in data["subcategories"]:
    for sub_key in range(len(data["subcategories"][key])):
        subcategories.append(data["subcategories"][key][sub_key])

subjects = data["subjects"]

subject_keys = subjects[:]
subcategory_keys = subcategories[:]
class_to_teach_keys = classes_to_teach[:]

jobs_collection = db.jobs
job_data = jobs_collection.find({'status': 0})
dict_list_raw_jobs = [document for document in job_data]
if(len(dict_list_raw_jobs) == 0):
    sys.exit(0)

tutors_collection = db.users
tutor_data = tutors_collection.find({"tutorUpdateFlag":"true"})
dict_list_raw_tutors = [document for document in tutor_data]
if(len(dict_list_raw_tutors) == 0):
    sys.exit(0)

###########################################################################################
# subcategory filter

job_subcats = get_job_subcats(dict_list_raw_jobs, subcategory_keys)
tutor_subcats = get_tutor_subcats(dict_list_raw_tutors, subcategory_keys)


###########################################################################################
# principal features filter
jobs = get_jobs(dict_list_raw_jobs, subject_keys, class_to_teach_keys)

job_ids_list = [
    bson.objectid.ObjectId(val['_id'])
    for val in dict_list_raw_jobs
]

tutors = get_tutors(dict_list_raw_tutors, subject_keys, class_to_teach_keys)

tutor_ids_list = [
    bson.objectid.ObjectId(val['_id'])
    for val in dict_list_raw_tutors
]

###########################################################################################
#location input from mongo

jobs_location_list = [
    list([val['locationLat'],val['locationLong']])
    for val in dict_list_raw_jobs
]

tutors_location_list = [
    list([val['locationLat'],val['locationLong']])
    for val in dict_list_raw_tutors
]

tutors_location_radius = [
    list([val['radius']])
    for val in dict_list_raw_tutors
]
tutors_location_radius = pd.DataFrame(tutors_location_radius)
r = np.array(tutors_location_radius)

## input from csv is akin to input from mongo query
## csv rows ---> collection documents
df_j_l = pd.DataFrame(jobs_location_list)
df_t_l = pd.DataFrame(tutors_location_list)
df_j_l = df_j_l.rename(index=str, columns={0: "latitude", 1: "longitude"})
df_t_l = df_t_l.rename(index=str, columns={0: "latitude", 1: "longitude"})
km2 = []
km2 = [haversine_np(df_j_l['longitude'],df_j_l['latitude'],
                    df_t_l['longitude'][i],df_t_l['latitude'][i]) for i in range(df_t_l.shape[0])]
km2 = np.array(km2)
km2 = np.transpose(km2)
distances = km2.copy()
r = r.reshape(1,r.shape[0])/1000
# a job has to be within 10 km from a tutor location and has to be within his defined range
# distances[distances <=10 and (distances-r)<=0]=1
# distances[distances!=1]=0
mask = np.all([(distances <= RANGE_LIMIT), ((distances-r) <= 0)], axis=0)
distances[mask] = 1
distances[~mask] = 0

###########################################################################################
#preferences matching using cosine similarities

matches = []
matches = distances
#cos_sims_subcats = cos_sim_bin(job_subcats, tutor_subcats, 'subcats')

#perform job-by-job (rows) serial filter pipelining instead of filter multiplication
#jobs.shape[0]
for i in range(jobs.shape[0]):
    # print(dict_list_raw_jobs[i])
    row = matches[i,:]
    # print('location: ' + str(row))
    ones_indices = np.where(row == 1)
    # print('ones_indices  ' + str(ones_indices))
    if(len(ones_indices[0]) == 0):
        continue
    # print(tutors[:,ones_indices[0]])
    # print(tutors[:,ones_indices[0]].shape)
    # print(jobs[i,:])
    # print(jobs[i,:].shape)
    tutor_subcats_curtailed_from_distances_filter = tutor_subcats[:, ones_indices[0]]
    result_row = cos_sim_bin(
        job_subcats[i,:].reshape(1,job_subcats[i,:].shape[0]), 
        tutor_subcats_curtailed_from_distances_filter, 
        'subcats')
    # print('row after subcats filter: ', str(result_row))
    for j in range(len(result_row[0])):
        #print('result row ' + str(result_row[0][j]))
        row[ones_indices[0][j]] = result_row[0][j]
    matches[i,:] = row
    # print('136', matches[i,:])
    ones_indices = np.where(row == 1)
    if(len(ones_indices[0]) == 0):
        continue
    tutors_curtailed_from_subcats_filter = tutors[:, ones_indices[0]]
    result_row = cos_sim_bin(
        jobs[i,:].reshape(1, jobs[i,:].shape[0]),
        tutors_curtailed_from_subcats_filter,
        'general')
    # print('row after general filter: ', str(result_row))
    for j in range(len(result_row[0])):
        #print('result_row ' + str(result_row[0][j]))
        row[ones_indices[0][j]] = result_row[0][j]
    matches[i, :] = row
    # print('\n\n\n')
#cos_sims = cos_sim_bin(jobs, tutors, 'general')
# cos_sims = cos_sim_bin(jobs, tutors_curtailed_from_subcat_filter, 'general')
# matches = distances*cos_sims*cos_sims_subcats
matches = pd.DataFrame(matches)


###########################################################################################
#prepare data to be written to mongodb database

dict_list_matches = []

states = np.where(matches==1)
matches_dict_keys = ['tutorId',  'jobId', 'previousStatus', 'currentStatus', 'dateOfStatusChange']
for i in range(len(states[0])):
    dict_list_matches.append(dict.fromkeys(matches_dict_keys, 0))
    dict_list_matches[i]['tutorId'] = tutor_ids_list[states[1][i]]
    dict_list_matches[i]['jobId'] = job_ids_list[states[0][i]]
    dict_list_matches[i]['dateOfStatusChange'] = datetime.datetime.now()
    dict_list_matches[i]['notified'] = 0
    # print(dict_list_matches[i])
matches_frame = pd.DataFrame(dict_list_matches)
matches_collection = db.matches
for i in range(len(dict_list_matches)):
    if(matches_collection.find_one({"$and": [{'tutorId': dict_list_matches[i]['tutorId']},
                                          {'jobId': dict_list_matches[i]['jobId']}]}) is None):
        jobId = dict_list_matches[i]['jobId']
        #check self-matching
        if(dict_list_matches[i]['tutorId'] ==
            jobs_collection.find_one({
                "_id": jobId
            })['userId']):
            dict_list_matches[i]['currentStatus'] = -1
        matches_collection.insert_one(dict_list_matches[i])
print('MATCHING COMPLETE')