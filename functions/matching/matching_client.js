var net = require('net');

var HOST = '127.0.0.1';
var PORT = 12345;
var message = '';

exports.initiateClient = function() {
    client = new net.Socket();

    client.connect(PORT, HOST, function() {
        console.log('Client connected to: ' + HOST + ':' + PORT);
    });

    client.on('data', function(data) {
        console.log('Client received: ' + data);
    });

    client.on('close', function() {
        console.log('Client closed');
    });

    client.on('error', function(err) {
        console.error(err);
    });
}

exports.sendSignal = function(string) {
    client.write(string);
}
