import pandas as pd
import numpy as np

def get_tutors(dict_list_raw_tutors, subject_keys, class_to_teach_keys):
    subjects_list = [
    list(val['preferredSubjects'])
    for val in dict_list_raw_tutors
    ]

    class_to_teach = [
        list(val['preferredClassToTeach'])
        for val in dict_list_raw_tutors
    ]

    dict_list_tutors = []

    for i in range(len(dict_list_raw_tutors)):
        dict_list_tutors.append(dict.fromkeys(subject_keys + class_to_teach_keys, 0))

    for i in range(len(dict_list_raw_tutors)):
        dict_list_tutors[i]['Salary'] = dict_list_raw_tutors[i]['minPreferredSalary']
        dict_list_tutors[i]['Gender Preference'] = dict_list_raw_tutors[i]['gender']

        for chunk in class_to_teach[i]:
            if chunk in dict_list_tutors[i]:
                dict_list_tutors[i][chunk]=1

        for chunk in subjects_list[i]:
            if chunk in dict_list_tutors[i]:
                dict_list_tutors[i][chunk]=1

    df = pd.DataFrame(dict_list_tutors)
    tutor_matrix = df.as_matrix()
    tutors = np.transpose(tutor_matrix)
    tutors[tutors=='m'] = 2
    tutors[tutors=='f'] = 1
    return tutors

def get_tutor_subcats(dict_list_raw_tutors, subcategory_keys):
    subcategory = [
        list(val['preferredSubcategory'])
        for val in dict_list_raw_tutors
    ]

    dict_list_tutors = []

    for i in range(len(dict_list_raw_tutors)):
        dict_list_tutors.append(dict.fromkeys(subcategory_keys, 0))

    for i in range(len(dict_list_raw_tutors)):
        for chunk in subcategory[i]:
            if chunk in dict_list_tutors[i]:
                #print(chunk)
                dict_list_tutors[i][chunk]=1

    #print(dict_list_tutors)
    df = pd.DataFrame(dict_list_tutors)
    tutor_matrix = df.as_matrix()
    tutor_subcats = np.transpose(tutor_matrix)
    
    return tutor_subcats