const user = require('../models/user');
const bcrypt = require('bcryptjs');
const auth = require('basic-auth');
const jwt = require('jsonwebtoken');
const config = require('../config/config.json');

exports.userExists = (req, res) => {
    const contactNumber = req.query.contactNumber;

    user.findOne({ contactNumber: contactNumber })
        .then(user => {
            if (user) {
                res.status(200).send({ message: 'True' });
            } else {
                res.status(200).send({ message: 'User not found !' });
            }
        })
        .catch(err => {
            res.status(500).send({ message: 'Internal Server Error !' });
        });
};

const loginUser = (contactNumber, deviceId) =>
    new Promise((resolve, reject) => {
        user.find({ contactNumber: contactNumber })

            .then(users => {
                if (users.length == 0) {
                    reject({ status: 404, message: 'User Not Found !' });
                } else {
                    return users[0];
                }
            })

            .then(users => {
                const userId = users._id;

                user.update(
                    { _id: userId },
                    { $set: { deviceIdToken: deviceId } }
                ).exec(function(err, editedUser) {
                    if (err) console.log(err);
                    else {
                        console.log(editedUser);
                        resolve({
                            status: 200,
                            message: users.email,
                            userId: users._id,
                            profilePicture: users.profilePicture,
                            fullName: users.fullName,
                            isTutor: users.isTutor,
                            institution: users.institution,
                            isVerified: users.isVerified,
                            tutorUpdateFlag: users.tutorUpdateFlag
                        });
                    }
                });

                // if (bcrypt.compareSync(password, hashedPassword)) {
                // 	console.log(deviceId);
                // 	user.update(
                // 					{ _id : userId},
                // 					{"$set": {deviceIdToken: deviceId}})
                // 			.exec(function(err, editedUser)
                // 			{
                // 					if(err) console.log(err);
                // 					else
                // 					{
                // 						console.log(editedUser);
                // 						resolve({ status: 200, message: email, userId: users._id, profilePicture: users.profilePicture, fullName: users.fullName, isTutor: users.isTutor, institution: users.institution, isVerified: users.isVerified, tutorUpdateFlag: users.tutorUpdateFlag });
                // 					}
                // 			});
                //
                // } else {
                //
                // 	reject({ status: 401, message: 'Invalid Credentials !' });
                // }
            })

            .catch(err =>
                reject({ status: 500, message: 'Internal Server Error !' })
            );
    });

exports.loginUser = loginUser;

exports.routerFunction = (req, res) => {
    const contactNumber = parseInt(req.headers['contact-number']);
    if (contactNumber) {
        loginUser(contactNumber, req.headers['device-id'])
            .then(result => {
                const token = jwt.sign(result, config.secret);

                res.status(result.status).json({
                    message: result.message,
                    token: token,
                    userId: result.userId.toString(),
                    path: result.profilePicture,
                    fullName: result.fullName,
                    isTutor: result.isTutor,
                    institution: result.institution.toString(),
                    isVerified: result.isVerified,
                    tutorUpdateFlag: result.tutorUpdateFlag
                });
            })

            .catch(err =>
                res.status(err.status).json({ message: err.message })
            );
    }
};
