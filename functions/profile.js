const user = require('../models/user');
const checkToken = require('./checkToken');
const mongoose = require('mongoose');
const dateDiff = require('date-diff');

exports.getProfile = function(req, res){
    if (checkToken.checkToken(req)) {
        user.findOne(
        	{
                _id: mongoose.Types.ObjectId(req.headers['user-id'])
            },
			{
				fullName: 1,
				email: 1,
				createdAt: 1,
				_id: 0,
				contactNumber: 1,
				location:1,
				profilePicture:1,
				gender:1,
				preferredSubjects:1,
				preferredDaysPerWeek:1,
				preferredSubcategory:1,
				preferredClassToTeach:1,
				minPreferredSalary:1,
				institution: 1,
				locationLat: 1,
				locationLong: 1,
				radius: 1,
				credentialPictures: 1,
				isVerifiedByVerificationImages: 1
			})
            .then(result => res.json(result))
            .catch(err => res.status(500).json({ message: 'Internal Server Error Fuck !' }));

    } else {

        res.status(401).json({ message: 'Invalid Token !' });
    }
};

exports.getProfileOthers = function(req, res){
	if(checkToken.checkToken(req)){
        user.findOne({
			_id: mongoose.Types.ObjectId(req.query.userId)
			}, {
        	fullName: 1, email: 1, createdAt: 1, _id: 0,
			contactNumber: 1, location:1, profilePicture:1, gender:1,
			preferredSubjects:1, preferredDaysPerWeek:1, preferredSubcategory:1,
			preferredClassToTeach:1, minPreferredSalary:1, institution: 1,
			locationLat: 1, locationLong: 1, radius: 1, credentialPictures: 1,
			isVerifiedByVerificationImages: 1})

            .then(result => {
                res.json(result);
            })

            .catch(err => reject({ status: 500, message: 'Internal Server Error Fuck !' }))
	}else{
		res.status(401).json({message: "Unauthorized"});
	}
};

exports.setTutorOrParent = function (request, response){
	if(checkToken.checkToken(request)){
		let userId = mongoose.Types.ObjectId(request.headers['user-id']);
		let flagValue = request.query.flagValue;
		try
		{
            user.findOneAndUpdate(
                    { _id : userId},
                    { "$set" : { isTutor: flagValue }})
                .exec(function(err, data){
                    if(err) console.log(err);
                    else{
                        response.json({ message: data.tutorUpdateFlag })
                    }
                });

			// if(flagValue === 'true'){
			// 	user
			// 			.update(
			// 					{ _id : userId},
			// 					{ "$set" : {isTutor: 'true'}})
			// 			.exec(function(err, editedUser)
			// 			{
			// 					if(err) console.log(err);
			// 					else
			// 					{
			// 							console.log(editedUser);
			// 							response.json("Successfully Verified !");
			// 							// request.next();
			// 					}
			// 			});
			// }
			// else{
			// 	user
			// 			.update(
			// 					{ _id : userId},
			// 					{ "$set" : {isTutor: 'false'}})
			// 			.exec(function(err, editedUser)
			// 			{
			// 					if(err) console.log(err);
			// 					else
			// 					{
			// 							console.log(editedUser);
			// 							response.json("Successfully Verified !");
			// 							// request.next();
			// 					}
			// 			});
			// }
		}
		catch(e){
				console.log(e.message);
				response.status(400).json({ message: "Oops something went wrong!" });
		}
	}
	else{
		response.status(401).json({message: 'Invalid Token !'});
	}
};

exports.checkPremium = function(request, response){
	if(checkToken.checkToken(request)){
        try{
            let userId = mongoose.Types.ObjectId(request.headers['user-id']);
            let dateOfPremiumStatus;
            let premiumStatus;
            let curDate;
            let diff;

            premiumPromise = user.findOne(
                { _id : userId })
                .exec(function(err, user)
                {
                    if(err) console.log(err.message);
                    else
                    {
                        //console.log(user);
                        premiumStatus = user.premiumStatus;
                        if(premiumStatus > 0){
                            dateOfPremiumStatus = new Date(user.dateOfPremiumStatus);
                            curDate = new Date();
                            diff = new dateDiff(curDate,dateOfPremiumStatus);
                            if(diff.months()>premiumStatus){
                                // console.log(dateOfPremiumStatus.toString());
                                // console.log(curDate.toString());
                                // console.log(diff.days());
                                // console.log(premiumStatus);
                                makePremiumStatusZero(userId);

                                console.log("Not premium - Set to 0");
                            }

                            console.log('Premium');
                        }
                        else{
                            console.log("Not premium");
                        }

                        response.json("Successful");
                        request.next();
                    }
                });
        }
        catch(e)
        {
                console.log(e.message);
                response.status(400).json({ message: "Oops something went wrong!" });
        }
	}
	else
	{
			response.status(401).json({ message: 'Invalid Token !' });
	}
};

exports.checkVerificationStatus = function(request, response){
	user.find({
		_id: mongoose.Types.ObjectId(request.query.userId)
	}, 'isVerifiedByVerificationImages')
		.exec(function(err, data){
			if(err){
				response.json({message: "Verification status check unsuccessful"});
			}else{
				response.json({message: data[0].isVerifiedByVerificationImages});
			}
		});
};

makePremiumStatusZero = function(userId){
	try
	{
		user.update(
						{ _id : userId},
						{ "$set" : {premiumStatus : 0, dateOfPremiumStatus: " "} })
				.exec(function(err, premiumStatus)
				{
						if(err) console.log(err);
						else
						{
							console.log("Premium Status changed successfully !");
						}
				});
	}
	catch(e)
	{
			console.log(e.message);
			response.status(400).json({ message: "Oops something went wrong!" });
	}
};
