var Match = require('../models/match')
var mongoose = require('mongoose');
var checkToken = require('../functions/checkToken');

exports.addMatchedJob = function(userId, jobId)
{
  try {
    var match = new Match({
        tutorId: userId,
        jobId: jobId
    });

    match.save(function(err, matchedJobData){
        if(err)
        {
            console.log(err.message);
        }
        else
        {
            console.log("Matched Job Posted Successfully - " + matchedJobData);
        }
    });
  } catch (e) {
    console.log("Exception - Adding Matched Job to User "+ e.stack);
  }
}

exports.getMatchedJobTutor = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            var userId = mongoose.Types.ObjectId(request.query.userId);

            Match
                .find(
                    { tutorId : userId })
                .populate('jobId')
                .exec(function(err, matchedJob)
                {
                    if(err) console.log(err.message);
                    else
                    {
                        console.log(matchedJob);

                        response.json(matchedJob);
                        request.next();
                    }
                });
        }
        catch(e)
        {
            console.log(e.message);
            response.status(400).json({ message: "Oops something went wrong!" });
        }
    }
    else
    {
        response.status(401).json({ message: 'Invalid Token !' });
    }
};

exports.getMatchedJobParent = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            var jobId = mongoose.Types.ObjectId(request.query.jobId);
            Match
                .find(
                    { jobId : jobId,
                        currentStatus : {$gte : 1} })
                .populate('tutorId')
                .exec(function(err, matchedJob)
                {
                    if(err) console.log(err.message);
                    else
                    {
                        console.log(matchedJob);

                        response.json(matchedJob);
                        request.next();
                    }
                });
        }
        catch(e)
        {
            console.log(e.message);
            response.status(400).json({ message: "Oops something went wrong!" });
        }
    }
    else
    {
        response.status(401).json({ message: 'Invalid Token !' });
    }
};

exports.createDummyMatch = function(tId, jId, pStatus, cStatus, date) {
    try {
        let m = new Match(
            {
                tutorId: mongoose.Types.ObjectId(tId),
                jobId: mongoose.Types.ObjectId(jId),
                previousStatus: pStatus,
                currentStatus: cStatus,
                dateOfStatusChange: date
            });

        m.save(function (err, savedMatch) {
            if (err) {
                console.log(err.toString());
            }
            else {
                console.log("Match Saved - " + savedMatch.tutorId);
            }
        });
    }
    catch (e) {
        console.log(e.toString());
    }
};

exports.createDummyMatchHeroku = function(req, res)
{
    try {
        console.log(req.body);

        let m = new Match(
            {
                tutorId: mongoose.Types.ObjectId(req.body.tutorId),
                jobId: mongoose.Types.ObjectId(req.body.jobId),
                previousStatus: req.body.previousStatus,
                currentStatus: req.body.currentStatus,
                dateOfStatusChange: req.body.date
            });

        m.save(function (err, savedMatch) {
            if (err) {
                console.log(err.toString());
            }
            else {
                console.log("Match Saved - " + savedMatch.tutorId);
                res.send("");
            }
        });
    }
    catch (e) {
        console.log(e.toString());
    }
}

exports.test = function()
{
    this.createDummyMatch("5a6dd75f85ba87223c942e2a", "5a708d9e558a1e1ca829a57a", 0, 0, new Date());
    this.createDummyMatch("5a6dd75f85ba87223c942e2a", "5a708da9558a1e1ca829a57b", 0, 1, new Date());
    this.createDummyMatch("5a6dd75f85ba87223c942e2a", "5a708dad558a1e1ca829a57c", 1, 2, new Date());
    this.createDummyMatch("5a6dd75f85ba87223c942e2a", "5a708db8558a1e1ca829a57d", 3, 4, new Date());
    this.createDummyMatch("5a6dd75f85ba87223c942e2a", "5a708dbd558a1e1ca829a57e", 3, 4, new Date());
    this.createDummyMatch("5a6dd75f85ba87223c942e2a", "5a708dc5558a1e1ca829a57f", 3, 4, new Date());
    this.createDummyMatch("5a6dd75f85ba87223c942e2a", "5a708dc9558a1e1ca829a580", 3, 4, new Date());
}
