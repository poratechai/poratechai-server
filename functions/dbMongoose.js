const mongoose = require('mongoose');
const bluebird = require('bluebird');

const options = {
    useNewUrlParser: true,
    useCreateIndex: true,

    poolSize: 10,                       //default for node.js app is 5
    promiseLibrary: bluebird,

    reconnectTries: Number.MAX_VALUE,
    reconnectInterval: 1000,

    keepAlive: 300000,

    connectTimeoutMS: 30000,
    socketTimeoutMS: 45000,
};

exports.connectToMlabDatabase = function(){
    // mongoose.connect("mongodb://localhost:27017/projectx");
    // mongoose.connect("mongodb://hyptastic001:the3men@ds161336.mlab.com:61336/projectx", options);
    mongoose.connect("mongodb://hyptastic001:the3men@ds229448.mlab.com:29448/projectx", options);

    mongoose.connection.on('connected', () => {
        console.log('Mongoose connection - successful');
    });

    mongoose.connection.on('error', (err) => {
        console.log('Mongoose connection error - ' + err);
    });

    mongoose.connection.on('disconnected', () => {
        console.log('Mongoose connection - disconnected');
    });

    // process.on('SIGINT', () => {
    //     mongoose.connection.close(() => {
    //         console.log('Mongoose connection - disconnected through app termination');
    //         process.exit(0);
    //     });
    // });
};
