const user = require('../models/user');

const logoutUser = (req, res) => {
  var userId = req.headers['user-id'];

  user.update(
    { _id : userId},
    { "$set" : {deviceIdToken: null}})
    .exec(function(err, user)
    {
      if (!user) {
        res.status(500);
        return;
      }
      if(err){
        console.log(err);
      }
      else{
        res.status(200).json('Successfully logged out !');
      }
    });
};

exports.logoutUser = logoutUser;
