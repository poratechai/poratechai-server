const soap = require('soap');
const random = require('random-js')();
const smsCode = require('../models/smsCode');
const user = require('../models/user');
const constants = require('../functions/constants');
const mongoose = require('mongoose');

//User Name - Mandatory
//Password - Mandatory
//Mobile Number - Mandatory
//Message - Mandatory
//Text Type - Mandatory
//Mask - Optional
//Campaign Name - Optional

const sendCode = (userId, contactNumber) =>{
  const updatedContactNumber = '0'+contactNumber;
  // console.log('Send code executed: ' + userId + ' ' + updatedContactNumber);
  const code = random.integer(1000,9999);

  // newSmsCode.save(function(err, data)
  // {
  //   if(err)
  //   {
  //     console.log(err.message);
  //   }
  //   else
  //   {
  //     console.log("Stored in SmsCode - " + data);
  //   }
  // });

  let args = {
    userName: constants.smsApiConstants.USER_NAME,
    userPassword: constants.smsApiConstants.USER_PASSWORD,
    mobileNumber: updatedContactNumber,
    smsText: 'Your Code: ' + code, type:'TEXT',
    maskName: '',
    campaignName: 'Porate Chai'
  };

  soap.createClient(constants.smsApiConstants.URL, function (err, client) {
      client.OneToOne(args, function (err, result) {
          if(err){
            console.log(err);
          }
          else{
            console.log(result);
            if(result){
              const newSmsCode = new smsCode({
                userId: userId,
                code: code
              });

              newSmsCode.save(function(err, data) {
                if(err){
                  console.log(err.message);
                } else {
                  console.log("Stored in SmsCode - " + data);
                }
              });
            }
          }
      });
  });
};

const verifyCode = (request, response) => {
  try {
      let userId = request.headers['user-id'];
      let code = request.query.code;

      smsCode.findOne({
          userId: mongoose.Types.ObjectId(userId),
          code: code
      })
          .exec()
          .then(
              (userCodePairResolved) => {
                  return user.updateOne({
                          _id: userId
                      }, {
                          "$set" : { isVerified: 'true' }
                      }).exec();
            })
          .then(
              () => {
                  response.status(200).json("Verified! Welcome to Porate Chai!");

                  smsCode.deleteMany({
                      userId: mongoose.Types.ObjectId(userId)
                  }).exec();
              }
          ).catch((e) => {
              console.log(e);

              response.status(401).json("Please try again");
      });
  }catch(e){
      console.log(e.message);
  }
};

const resendCode = (req, res) => {
  let userId = req.headers['user-id'];

  user.findOne({
      _id: userId
  })
      .exec()
      .then(
          (userDataResolved) => {
              sendCode(userId, userDataResolved.contactNumber);
              setTimeout(() => {
                  res.json("Code resent");
              }, 4000);
          })
      .catch((e) => {
          console.log(e);
  });
};

const sendNewPassword = (contactNumber, newPassword) =>{
  let args = {
    userName: constants.smsApiConstants.USER_NAME,
    userPassword: constants.smsApiConstants.USER_PASSWORD,
    mobileNumber: contactNumber,
    smsText: 'Your New Password: ' + newPassword, type:'TEXT',
    maskName: '',
    campaignName: 'Porate Chai'
  };

  return new Promise(function(resolve, reject) {
    soap.createClient(constants.smsApiConstants.URL, function (err, client) {
      client.OneToOne(args, function (err, result) {
        if(err){
          console.log(err);
          reject(err);
        }
        else{
          if(result){
            resolve(result);
          }
        }
      });
    });
  });
};

exports.sendCode = sendCode;
exports.verifyCode = verifyCode;
exports.resendCode = resendCode;
exports.sendNewPassword = sendNewPassword;
