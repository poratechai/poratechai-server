const mongoose = require('mongoose');
const checkToken = require('../functions/checkToken');
let User = require('../models/user');
let ReviewTutor = require('../models/reviewTutor');
let ReviewParent = require('../models/reviewParent');
let Match = require('../models/match');
let Job = require('../models/job');

exports.getTutorStats = function(req, res){
    if(checkToken.checkToken(req)){
        try{
            console.log('asking for tutor stats');
            let userId = mongoose.Types.ObjectId(req.headers['user-id']);
            let stats = {
                activeContracts: 0,
                currentIncome: 0,
                rating: 0,
                matches: 0
            }
            //get number of active contacts and current income
            Job.find({tutorId: userId, status: 4})
                .select('salary tutorId')
                .exec(function(err, data){
                    let salary = 0;
                    data.forEach(function(item){
                        salary += item.salary;
                    })
                    stats.activeContracts = data.length;
                    stats.currentIncome = salary;

                    ReviewTutor.find({userId: userId})
                        .exec(function(err, data){
                            let rating = 0;
                            data.forEach(function(item){
                                rating += item.rating;
                            })
                            rating = rating/data.length;
                            stats.rating = rating;

                            Match.find({tutorId: userId})
                                .exec(function(err, data){
                                    stats.matches = data.length;
                                    res.send(stats);
                                })
                        })
                })
        } catch(e) {
            console.log(e.message);
            res.status(400).json({ message: "Oops something went wrong!" });
        }
    } else {
        res.status(401).json({'Response': 'Invalid Token!'});
    }
}

exports.getParentStats = function(req, res){
    if(checkToken.checkToken(req)){
        try{
            console.log('asking for parent stats');
            let userId = mongoose.Types.ObjectId(req.headers['user-id']);
            let stats = {
                postedJobs: 0,
                contractRequests: 0,
                rating: 0,
                tutorsEmployed: 0
            }

            //get number of posted jobs
            Job.find({userId: userId})
                .exec(function(err,data){
                    stats.postedJobs = data.length;

                    //get number of contract requests
                    Match.find({currentStatus: { $gt: 0 }})
                        .populate({
                            path: 'jobData'
                        })
                        .exec(function(err, data){
                            let count = 0;
                            data.map((one) => {
                                if(one.jobData[0].userId == req.headers['user-id']) {
                                    count++;
                                }
                            })
                            stats.contractRequests = count;
                            

                            //get rating of parent
                            ReviewParent.find({userId: userId})
                                .exec(function(err, data){
                                    let rating = 0;
                                    data.forEach(function(item){
                                        rating += item.rating;
                                    })
                                    rating = rating/data.length;
                                    stats.rating = rating;

                                    //get number of tutors employed
                                    Job.find({userId: userId})
                                        .exec(function(err, data){
                                            let count = 0;
                                            data.forEach(function(one){
                                                if(one.tutorId != null){
                                                    count++;
                                                }
                                                stats.tutorsEmployed = count;
                                            })
                                            res.send(stats);
                                        })
                                })
                        })
                })
        } catch(e){
            console.log(e.message);
            res.status(400).json({message: "Oops something went wrong!"});
        }
    } else {
        res.status(401).json({'Response': 'Invalid Token!'});
    }
}