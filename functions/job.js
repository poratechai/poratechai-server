let Job = require('../models/job');
let Calendar = require('../models/calendar');
let User = require('../models/user');
let Match = require('../models/match');
let userFunctionImported = require('../functions/user');
let calendarFunctionImported = require('../functions/calendar');
let checkToken = require('../functions/checkToken');
let matchingClient = require('./matching/matching_client');
let Promise = require('bluebird');
let mongoose = Promise.promisifyAll(require('mongoose'));

//Parent/Student - start

exports.saveJob = function(request, response) {
    if(checkToken.checkToken(request)){
        try{
            let jobData = request.body;

            let job = new Job({
                jobTitle : jobData.jobTitle,
                category : jobData.category,
                subcategory : jobData.subcategory,
                class : jobData.class,
                subjects : jobData.subjects,
                tutorGenderPreference : jobData.tutorGenderPreference,
                locationLat : jobData.locationLat,
                locationLong : jobData.locationLong,
                location : jobData.location,
                salary : jobData.salary,
                daysPerWeek : jobData.daysPerWeek,
                userId : mongoose.Types.ObjectId(jobData.userId)
            });

            job.save(function(err, jobData){
                if(err) {
                    console.log(err.message);
                    response.json(err.stack);
                } else {
                    userFunctionImported.addJobToUser(jobData.userId, jobData._id);
                    calendarFunctionImported.createCalendar(jobData._id);

                    console.log("Job Posted Successfully - " + jobData.id);

                    response.json("Job Posted Successfully");

                    // send signal to Python server
                    matchingClient.sendSignal('run 1 matching');
                }
            });
        } catch(e) {
            console.log(e.message);
            response.status(400).json({ message: "Oops something went wrong!" });
        }
    } else {
        response.status(401).json(({ message: 'Invalid Token !' }));
    }
};

//Get a single posted job and its matches based on jobId

exports.getSinglePostedJobAndMatches = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let jobId = mongoose.Types.ObjectId(request.query.jobId);

            Job
                .findOne(
                    { _id : jobId })
                .populate({
                    path : 'matchedTutors',
                    match : {
                        $and : [ { currentStatus : { $lte : 3 } }, { currentStatus : { $gte : 1 } } ]
                    },
                    populate : {
                        path : 'tutorProfile'
                    }
                })
                .exec(function(err, data){
                    if(err)
                    {
                        console.log(err);
                    }
                    else {
                        response.json(data);
                    }
                });

        }
        catch(e)
        {
            console.log(e.message);
            response.status(400).json({ message: "Oops something went wrong!" });
        }
    }
    else
    {
        response.status(401).json({ message: 'Invalid Token !' });
    }
};

//Get posted jobs and their matches for a parent/student

exports.getPostedJobsAndMatches = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let userId = mongoose.Types.ObjectId(request.headers['user-id']);

            User
                .findOne(
                    { _id : userId }, 'fullName')
                .populate({
                    path : 'postedJobData',
                    match : { 'status' : 0 },
                    populate : {
                        path : 'matchedTutors',
                        match : {
                            $and : [ { currentStatus : { $lte : 3 } }, { currentStatus : { $gte : 1 } } ]
                        },
                        populate : {
                            path : 'tutorProfile'
                        }
                    }
                })
                .exec(function(err, data){
                    if(err)
                    {
                        console.log(err);
                    }
                    else
                    {
                        response.json(data.postedJobData);
                    }
                });

        }
        catch(e)
        {
            console.log(e.message);
            response.status(400).json({ message: "Oops something went wrong!" });
        }
    }
    else
    {
        response.status(401).json({ message: 'Invalid Token !' });
    }
};

//Get a single active posted job

exports.getSingleActivePostedJob = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let jobId = mongoose.Types.ObjectId(request.query.jobId);

            Job
                .findOne(
                    { _id : jobId, status : 4 })
                .populate({ path : 'reviewTutor' })
                .populate({ path : 'tutorProfile' })
                .exec(function(err, data){
                    if(err)
                    {
                        console.log(err);
                    }
                    else
                    {
                        response.json(data);
                    }
                });

        }
        catch(e)
        {
            console.log(e.message);
            response.status(400).json({ message: "Oops something went wrong!" });
        }
    }
    else
    {
        response.status(401).json({ message: 'Invalid Token !' });
    }
};

//Get active posted jobs for a parent/student

exports.getActivePostedJobs = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let userId = mongoose.Types.ObjectId(request.headers['user-id']);

            User.findOne(
                { _id : userId }, 'fullName')
                .populate({
                    path : 'postedJobData',
                    match : { 'status' : 4 },
                    populate : [ { path : 'reviewTutor' }, { path : 'tutorProfile' } ]
                })
                .exec(function(err, userJobs){
                    if(err) console.log(err.message);
                    else
                    {
                        response.json(userJobs.postedJobData);
                    }
                })
        }
        catch(e)
        {
            console.log(e.message);
            response.status(400).json({ message : 'Oops something went wrong!'});
        }
    }
    else
    {
        response.status(401).json({ message: 'Invalid Token !' });
    }
};

//Get a single posted job history

exports.getSinglePostedJobHistory = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let jobId = mongoose.Types.ObjectId(request.query.jobId);

            Job
                .findOne(
                    { _id : jobId, status : 5 })
                .populate({ path : 'reviewTutor' })
                .populate({ path : 'tutorProfile' })
                .exec(function(err, data){
                    if(err)
                    {
                        console.log(err);
                    }
                    else
                    {
                        response.json(data);
                    }
                });

        }
        catch(e)
        {
            console.log(e.message);
            response.status(400).json({ message: "Oops something went wrong!" });
        }
    }
    else
    {
        response.status(401).json({ message: 'Invalid Token !' });
    }
};

//Get posted job history for a parent/student

exports.getPostedJobHistory = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let userId = mongoose.Types.ObjectId(request.headers['user-id']);

            User.findOne({ _id : userId }, 'fullName')
                .populate({
                    path : 'postedJobData',
                    match : { 'status' : 5 },
                    populate : [ { path : 'reviewTutor' }, { path : 'tutorProfile' } ]
                })
                .exec(function(err, userJobs){
                    if(err) console.log(err.message);
                    else
                    {
                        response.json(userJobs.postedJobData);
                    }
                })
        }
        catch(e)
        {
            console.log(e.message);
        }
    }
    else
    {
        response.status(401).json({ message : ' Invalid Token ! '});
    }
};

exports.updateJob = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let jobId = mongoose.Types.ObjectId(request.query.jobId);

            Job
                .update(
                    { _id : jobId},
                    { "$set" : request.body})
                .exec(function(err, editedJob)
                {
                    if(err) console.log(err);
                    else
                    {
                        response.json("Job Updated Successfully");

                        // send signal to Python server
                        matchingClient.sendSignal('run 1 matching');
                    }
                });
        }
        catch(e)
        {
            console.log(e.message);
            response.status(400).json({ message: "Oops something went wrong!" });
        }
    }
    else
    {
        response.status(401).json({ message: 'Invalid Token !' });
    }
};

exports.deleteJob = function(request, response)
{
    if(checkToken.checkToken(request)) {
        try {
            let jobId = mongoose.Types.ObjectId(request.query.jobId);

            Match
                .remove({ jobId : jobId })
                .exec()
                .then(() => {
                    return Calendar.remove({ jobId: jobId }).exec();
                })
                .then(() => {
                    return Job.remove({ _id : jobId }).exec();
                })
                .then(() => {
                    response.status(200).json("Job Deleted Successfully");
                })
                .catch((err) => {
                    if(err) console.log(err);
                })
        } catch(e) {
            console.log(e.message);
            response.status(400).json({ message: "Oops something went wrong!" });
        }
    } else {
        response.status(401).json({ message: 'Invalid Token !' });
    }
};

//Parent/Student - end

//Tutor - start

//Get single job match for a tutor

exports.getSingleMatchedJob = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let matchId = mongoose.Types.ObjectId(request.query.matchId);

            Match.findOne(
                {
                    _id : matchId,
                    currentStatus: 0
                })
                .populate({
                    path : 'jobData',
                    populate : {
                        path : 'userProfile'
                    }
                })
                .exec(function(err, matchData){
                    if(err) console.log(err.message);
                    else
                    {
                        response.json(matchData);
                    }
                })
        }
        catch(e)
        {
            console.log(e);
        }
    }
    else
    {
        response.status(400).json({message : 'Invalid Token !'});
    }
};

//Get job matches for a tutor

exports.getMatchedJobs = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let userId = mongoose.Types.ObjectId(request.headers['user-id']);

            Match.find(
                {
                    tutorId : userId,
                    currentStatus: 0
                })
                .populate({
                    path : 'jobData',
                    populate : {
                        path : 'userProfile'
                    }
                })
                .exec(function(err, userJobs){
                    if(err) console.log(err.message);
                    else
                    {
                        response.json(userJobs);
                    }
                })
        }
        catch(e)
        {
            console.log(e);
        }
    }
    else
    {
        response.status(400).json({message : 'Invalid Token !'});
    }
};

//Get single applied job for a tutor

exports.getSingleAppliedJob = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let matchId = mongoose.Types.ObjectId(request.query.matchId);

            Match.findOne(
                {
                    _id : matchId,
                    $and: [ { currentStatus: { $lte: 3 } }, { currentStatus: { $gte: 1 } } ]
                })
                .populate({
                    path : 'jobData',
                    populate : {
                        path : 'userProfile'
                    }
                })
                .exec(function(err, matchData){
                    if(err) console.log(err.message);
                    else
                    {
                        response.json(matchData);
                    }
                })
        }
        catch(e)
        {
            console.log(e);
        }
    }
    else
    {
        response.status(400).json({message : 'Invalid Token !'});
    }
};

//Get applied jobs for a tutor

exports.getAppliedJobs = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let userId = mongoose.Types.ObjectId(request.headers['user-id']);

            Match.find(
                {
                    tutorId : userId,
                    currentStatus : { $gt : 0, $lt: 4 }
                })
                .populate({
                    path : 'jobData',
                    populate : {
                        path : 'userProfile'
                    }
                })
                .exec(function(err, userJobs){
                    if(err) console.log(err.message);
                    else
                    {
                        response.json(userJobs);
                    }
                })
        }
        catch(e)
        {
            console.log(e);
        }
    }
    else
    {
        response.status(400).json({message : 'Invalid Token !'});
    }
};

//Get single active job for a tutor

exports.getSingleActiveJob = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let jobId = mongoose.Types.ObjectId(request.query.jobId);

            Job.findOne({ _id : jobId, status : 4 })
                .populate({ path : 'reviewParent' })
                .populate({ path : 'userProfile'})
                .exec(function(err, userJobs){
                    if(err) console.log(err.message);
                    else
                    {
                        response.json(userJobs);
                    }
                })
        }
        catch(e)
        {
            console.log(e);
        }
    }
    else
    {
        response.status(400).json({message : 'Invalid Token !'});
    }
};

//Get active jobs for a tutor

exports.getActiveJobs = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let userId = mongoose.Types.ObjectId(request.headers['user-id']);

            User.findOne({ _id : userId }, 'fullName')
                .populate({
                    path : 'jobHistory',
                    match : { status : 4 },
                    populate : [ { path : 'reviewParent' }, { path : 'userProfile'} ]
                })
                .exec(function(err, userJobs){
                    if(err) console.log(err.message);
                    else
                    {
                        response.json(userJobs.jobHistory);
                    }
                })
        }
        catch(e)
        {
            console.log(e);
        }
    }
    else
    {
        response.status(400).json({message : 'Invalid Token !'});
    }
};

//Get single job history for a tutor

exports.getSingleJobHistory = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let jobId = mongoose.Types.ObjectId(request.query.jobId);

            Job.findOne({ _id : jobId, status : 5 })
                .populate({ path : 'reviewParent' })
                .populate({ path : 'userProfile'})
                .exec(function(err, userJobs){
                    if(err) console.log(err.message);
                    else
                    {
                        response.json(userJobs);
                    }
                })
        }
        catch(e)
        {
            console.log(e);
        }
    }
    else
    {
        response.status(400).json({message : 'Invalid Token !'});
    }
};

//Get job history for a tutor

exports.getJobHistory = function(request, response)
{
    if(checkToken.checkToken(request))
    {
        try
        {
            let userId = mongoose.Types.ObjectId(request.headers['user-id']);

            User.findOne({ _id : userId }, 'fullName')
                .populate({
                    path : 'jobHistory',
                    match : { status : 5 },
                    populate : [ { path : 'reviewParent' }, { path : 'userProfile' } ]
                })
                .exec(function(err, userJobs){
                    if(err) console.log(err.message);
                    else
                    {
                        response.json(userJobs.jobHistory);
                    }
                })
        }
        catch(e)
        {
            console.log(e);
        }
    }
    else
    {
        response.status(400).json({message : 'Invalid Token !'});
    }
};

//Tutor - end

exports.addReviewIdToJobForParent = function(reviewId, jobId)
{
    try
    {
        Job.update(
            { _id : mongoose.Types.ObjectId(jobId) },
            { "$set" : { reviewIdParent : mongoose.Types.ObjectId(reviewId) }}
        ).exec(function(err, reviewData)
        {
            if(err)
            {
                console.log(err);
            }
            else
            {
                console.log("Review added to job successfully - For Parent - " + reviewData);
            }
        })
    }
    catch(e)
    {
        console.log(e.message);
    }
};

exports.addReviewIdToJobForTutor = function(reviewId, jobId)
{
    try
    {
        Job.update(
            { _id : mongoose.Types.ObjectId(jobId) },
            { "$set" : { reviewIdTutor : mongoose.Types.ObjectId(reviewId) }}
        ).exec(function(err, review)
        {
            if(err)
            {
                console.log(err);
            }
            else
            {
                console.log("Review added to job successfully - For Tutor - " + review);
            }
        })
    }
    catch(e)
    {
        console.log(e.message);
    }
};
