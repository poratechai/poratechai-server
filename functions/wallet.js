//Ratul - start

let Wallet          = require('../models/wallet');
let User            = require('../models/user');
let Transaction     = require('../models/transaction');
let moment          = require('moment');
let Promise         = require('bluebird');
let mongoose        = require('mongoose');
mongoose.Promise    = Promise;
let rp              = require('request-promise');
let constants       = require('../functions/constants');
let md5             = require('md5');
let checkToken      = require('../functions/checkToken');

let currentWalletFunction = require('../functions/wallet');

exports.createWallet = function(usrId){
    console.log("Wallet Creation");

    let wallet = new Wallet({
        userId              :   usrId,
        // contractBalance  :   0,
        subscriptionMonths  :   3, //3 months for promotional period
        updateDate          :   Date.now()
    });

    console.log("Creating Wallet");

    let promiseWallet = wallet.save();

    return promiseWallet;
}

exports.checkPending = function(req, res){
    if(checkToken.checkToken(req)){
        let data = req.query;

        Transaction.find({
            userId  : req.headers['user-id'],
            status  : constants.transactionStatus.PENDING
        }).exec(function(err, transactionData){
            if(err){
                console.log(err);
            }else{
                if(transactionData!=null){
                    res.send(transactionData[0].invoiceNumber);
                }else{
                    res.send("");
                }
            }
        });
    }else{
        res.status(401).send("Unauthorized");
    }
}

exports.createInvoice = function(req, res){
    if(checkToken.checkToken(req)){
        let data = req.body;

        console.log("Invoice creation - " + req.headers['user-id']);

        Transaction.find({
            userId  : req.headers['user-id'],
            status  : constants.transactionStatus.PENDING
        }).then((transactionData) => {
            if(!transactionData.length){
                console.log("No pending transaction has been found");
                console.log("Creating new invoice");

                let invoiceData = {
                    'amount'                : data.amount,
                    'product_name'          : data.product_name,
                    'product_description'   : data.product_description,
                    'name'                  : data.name,
                    'email'                 : data.email,
                    'phone'                 : data.phone,
                    'address'               : data.address,
                    'city'                  : data.city,
                    'state'                 : data.state,
                    'zipcode'               : data.zipcode,
                    'country'               : data.country
                };

                let timestamp = Math.floor(new Date().getTime()/1000);

                invoiceData['app_key']      = constants.portwalletConnection.APP_KEY;
                invoiceData['timestamp']    = timestamp;
                invoiceData['token']        = generateToken(timestamp);
                invoiceData['call']         = constants.portwalletConnection.CALL.GEN_INVOICE;
                invoiceData['currency']     = constants.portwalletConnection.CURRENCY;
                invoiceData['redirect_url'] = constants.portwalletConnection.REDIRECT_URL;

                let options = {
                    method      : 'POST',
                    uri         : constants.portwalletConnection.END_POINT,
                    formData    : invoiceData,
                    headers     : {
                        'content-type'    : 'application/x-www-form-urlencoded'
                    }
                };

                return rp(options);
            }else{
                console.log("Pending transaction has been found - " + transactionData[0].invoiceNumber);

                req.query.invoice_id = transactionData[0].invoiceNumber;
                currentWalletFunction.getInvoice(req, res);

                throw new Error('Pending transaction has been found');
            }
        }).then((newInvoice) => {
            console.log(newInvoice);

            let newInvoiceParsed = JSON.parse(newInvoice);

            console.log(newInvoiceParsed.status);

            if(newInvoiceParsed.status===200){
                console.log("New invoice has been created on the PortWallet server");
                console.log("Creating new transaction on PorateChai server");

                let newTransaction = new Transaction({
                    'userId'          : req.headers['user-id'],
                    'invoiceNumber'   : newInvoiceParsed.data.invoice_id,
                    'amount'          : newInvoiceParsed.data.amount,
                    'oneMonthCost'    : data.oneMonthCost,
                    'threeMonthsCost' : data.threeMonthsCost,
                    'updateDate'      : new Date(),
                    'status'          : constants.transactionStatus.PENDING
                });

                return newTransaction.save().then((newlySavedTransaction) => {
                    return Promise.resolve([newlySavedTransaction, newInvoiceParsed]);
                });
            }else{
                console.log("Something went wrong!");
                res.send("Something went wrong!");
            }
        }).then(([newlySavedTransaction, newInvoiceParsed]) => {
            console.log("New transaction has been created");
            console.log("Adding new transaction to wallet");

            Wallet.update({
                userId  : req.headers['user-id']
            },{
                $push   : { transactions    : newlySavedTransaction._id }
            }).exec(function(err){
                if(err){
                    console.log(err);
                }else{
                    console.log("Transaction added to wallet.");

                    res.json(newInvoiceParsed);
                }
            });
        }).catch((err) => {
            console.log(err.message);
        });
    }else{
        res.status(401).send("Unauthorized");
    }
};

exports.updateWalletV1 = function(req, res){
    if(checkToken.checkToken(req)){
        let data = req.query;

        let invoiceData = {
            'invoice'   : ""
        };

        let timestamp = Math.floor(new Date().getTime()/1000);

        invoiceData['app_key']      = constants.portwalletConnection.APP_KEY;
        invoiceData['timestamp']    = timestamp;
        invoiceData['token']        = generateToken(timestamp);
        invoiceData['call']         = constants.portwalletConnection.CALL.GET_INVOICE;

        let pendingInvoiceDetails = { };

        let options = {
            method      : 'POST',
            uri         : constants.portwalletConnection.END_POINT,
            formData    : invoiceData,
            headers     : {
                'content-type'  : 'application/x-www-form-urlencoded'
            }
        };

        Transaction.find({
            userId  : req.headers['user-id'],
            status  : constants.transactionStatus.PENDING
        }).then((transactionData) => {
            let promiseArray = transactionData.map((transaction) => {
                invoiceData.invoice = transaction.invoiceNumber;

                options.formData = invoiceData;

                return rp(options);
            });

            return Promise.all(promiseArray);
        }).then((invoiceResults) => {
            let promiseArray = invoiceResults.map((invoice) => {
                let invoiceParsed = JSON.parse(invoice);

                if(invoiceParsed.status===200){
                    return Transaction.update({
                        'invoiceNumber' : invoiceParsed.data.invoice_id
                    },{
                        $set    : { 'status' : constants.transactionStatus.CONFIRMED }
                    },{
                        multi   : true
                    }).exec();
                }else{
                    pendingInvoiceDetails = invoiceParsed;
                }
            });

            return Promise.all(promiseArray);
        }).then(() => {
            return Wallet.find({
                userId  :   req.headers['user-id']
            }).populate({
                path    : 'transactions',
                match   : { status : constants.transactionStatus.CONFIRMED }
            });
        }, (rejected) => {
            return Wallet.find({
                userId  :   req.headers['user-id']
            }).populate({
                path    : 'transactions',
                match   : { status : constants.transactionStatus.CONFIRMED }
            });
        }).then((walletData) => {
            let totalContractTokens = walletData[0].contractBalance + walletData[0].transactions.map((transaction) => {
                return transaction.amount/constants.walletConstants.UNIT_PRICE;
            }).reduce((total, currentVal) => {
                return total + currentVal;
            });

            return Wallet.update({
                userId  : req.headers['user-id']
            },{
                $set    : { contractBalance : totalContractTokens, lastPaidDate : new Date() }
            }).exec().then(() => {
                return Promise.resolve(walletData);
            });
        }).then((walletData) => {
            let promiseArray = walletData[0].transactions.map((transaction) => {
                return transaction._id;
            }).map((transactionId) => {
                return Transaction.update({
                    _id     : transactionId
                },{
                    $set    : { status : constants.transactionStatus.CONFIRM_PAID }
                }).exec();
            });

            return Promise.all(promiseArray);
        }).then(() => {
            return Wallet.find({
                userId  : req.headers['user-id']
            });
        }, () => {
            return Wallet.find({
                userId  : req.headers['user-id']
            });
        }).then((walletData) => {
            console.log(walletData[0]);

            let updateResponse = {
                balance                 : walletData[0].contractBalance,
                pendingInvoice          : pendingInvoiceDetails
            };

            res.json(updateResponse);
        }).catch((e) => {
            console.log("Something went wrong!");

            res.end();
        });
    }else{
        res.status(401).send("Unauthorized");
    }
};

exports.updateWalletV2 = function(req, res){
    if(checkToken.checkToken(req)){
        let data = req.query;

        let invoiceData = {
            'invoice'   : ""
        };

        let timestamp = Math.floor(new Date().getTime()/1000);

        invoiceData['app_key']      = constants.portwalletConnection.APP_KEY;
        invoiceData['timestamp']    = timestamp;
        invoiceData['token']        = generateToken(timestamp);
        invoiceData['call']         = constants.portwalletConnection.CALL.GET_INVOICE;

        let pendingInvoiceDetails = { };

        let options = {
            method      : 'POST',
            uri         : constants.portwalletConnection.END_POINT,
            formData    : invoiceData,
            headers     : {
                'content-type'  : 'application/x-www-form-urlencoded'
            }
        };

        Transaction.find({
            userId  : req.headers['user-id'],
            status  : constants.transactionStatus.PENDING
        }).then((transactionData) => {
            let promiseArray = transactionData.map((transaction) => {
                invoiceData.invoice = transaction.invoiceNumber;

                options.formData = invoiceData;

                return rp(options);
            });

            return Promise.all(promiseArray);
        }).then((invoiceResults) => {
            let promiseArray = invoiceResults.map((invoice) => {
                let invoiceParsed = JSON.parse(invoice);

                if(invoiceParsed.status===200){
                    return Transaction.update({
                        'invoiceNumber' : invoiceParsed.data.invoice_id
                    },{
                        $set    : { 'status' : constants.transactionStatus.CONFIRMED }
                    },{
                        multi   : true
                    }).exec();
                }else{
                    pendingInvoiceDetails = invoiceParsed;
                }
            });

            return Promise.all(promiseArray);
        }).then(() => {
            return Wallet.find({
                userId  :   req.headers['user-id']
            }).populate({
                path    : 'transactions',
                match   : { status : constants.transactionStatus.CONFIRMED }
            });
        }, (rejected) => {
            return Wallet.find({
                userId  :   req.headers['user-id']
            }).populate({
                path    : 'transactions',
                match   : { status : constants.transactionStatus.CONFIRMED }
            });
        }).then((walletData) => {
            let subscriptionStatus = walletData[0].subscriptionMonths + walletData[0].transactions.map((transaction) => {
                if(transaction.amount === transaction.oneMonthCost){
                    return constants.subscriptionStatus.DURATION_ONE_MONTH;
                }else if(transaction.amount === transaction.threeMonthsCost){
                    return constants.subscriptionStatus.DURATION_THREE_MONTHS
                }else{
                    return 0;
                }
            }).reduce((total, currentValue) => {
                return total + currentValue;
            });

            if(walletData[0].subscriptionMonths === constants.subscriptionStatus.DURATION_NONE){ //user is newly subscribed
                return Wallet.update({
                    userId  : req.headers['user-id']
                },{
                    $set    : { subscriptionMonths : subscriptionStatus, lastPaidDate : new Date() }
                }).exec().then(() => {
                    return Promise.resolve(walletData);
                });
            }else{  //user already has older subscription - lastPaidDate is not changed
                console.log(subscriptionStatus);
                return Wallet.update({
                    userId  : req.headers['user-id']
                },{
                    $set    : { subscriptionMonths : subscriptionStatus }
                }).exec().then(() => {
                    return Promise.resolve(walletData);
                });
            }
        }).then((walletData) => {
            let promiseArray = walletData[0].transactions.map((transaction) => {
                return transaction._id;
            }).map((transactionId) => {
                return Transaction.update({
                    _id     : transactionId
                },{
                    $set    : { status : constants.transactionStatus.CONFIRM_PAID }
                }).exec();
            });

            return Promise.all(promiseArray);
        }).then(() => {
            return Wallet.find({
                userId  : req.headers['user-id']
            });
        }, () => {
            return Wallet.find({
                userId  : req.headers['user-id']
            });
        }).then((walletData) => {
            return currentWalletFunction.checkSubscriptionValidity(walletData[0]);
        }).then(()=>{ //subscription months update resolved - invalid membership
            let updateResponse = {
                subscriptionType        : constants.subscriptionStatus.DURATION_NONE,
                validityInDays          : 0.0,
                pendingInvoice          : pendingInvoiceDetails
            };

            res.json(updateResponse);
        },(walletData)=>{ //subscription months update rejected - valid membership
            let updateResponse = {
                subscriptionType        : walletData.subscriptionMonths,
                validityInDays          : walletData.validityInDays,
                pendingInvoice          : pendingInvoiceDetails
            };
            console.log("Subscription is still valid - Wallet has not been updated - ");

            res.json(updateResponse);
        }).catch((e) => {
            console.log(e);

            console.log("Something went wrong!");

            res.end();
        });
    }else{
        res.status(401).send("Unauthorized");
    }
};

exports.getInvoice = function(req, res){
    if(checkToken.checkToken(req)){
        let data = req.query;

        let invoiceData = {
            'invoice'   : data.invoice_id
        };

        let timestamp = Math.floor(new Date().getTime()/1000);

        invoiceData['app_key']      = constants.portwalletConnection.APP_KEY;
        invoiceData['timestamp']    = timestamp;
        invoiceData['token']        = generateToken(timestamp);
        invoiceData['call']         = constants.portwalletConnection.CALL.GET_INVOICE;

        let options = {
            method      : 'POST',
            uri         : constants.portwalletConnection.END_POINT,
            formData    : invoiceData,
            headers     : {
                'content-type'  : 'application/x-www-form-urlencoded'
            }
        };

        rp(options).then((invoiceResponse) => {
            let invoiceResponseParsed = JSON.parse(invoiceResponse);

            res.json(invoiceResponseParsed);
        });
    }else{
        res.status(401).send("Unauthorized");
    }
};

exports.cancelInvoice = function(req, res){
    if(checkToken.checkToken(req)){
        let invoiceNumberToRemove = req.query.invoiceNumber;

        console.log("Invoice Number to Remove - " + invoiceNumberToRemove);

        Transaction.update({
            invoiceNumber   : invoiceNumberToRemove
        }, {
            $set            : { status  : constants.transactionStatus.CANCELLED }
        },function(err){
            if(err) {
                console.log(err);
            }else{
                res.send({message: 'Invoice deleted successfully'});
            }
        });
    }else{
        res.status(401).send("Unauthorized");
    }
};

exports.checkSubscriptionValidity = function(walletData){
    try{
        let promiseToUpdateSubscriptionMonths=null;

        let promiseToCheckValidity = new Promise(function(resolve, reject){
            let validityDays = (moment(walletData.lastPaidDate).add(walletData.subscriptionMonths, 'month')).diff(moment().subtract(1, 'day'), 'days', false);

            if(validityDays>0.0){
                walletData.validityInDays = validityDays;
                reject(walletData);    //reject update - membership is still valid
            }else if(walletData.subscriptionMonths>constants.subscriptionStatus.DURATION_NONE){      //validity is 0 days but month is already 0
                walletData.validityInDays = validityDays;
                resolve(walletData);

                promiseToUpdateSubscriptionMonths = Wallet.update({     //execute update - membership is invalid
                    _id     : walletData._id
                },{
                    $set    : { subscriptionMonths: constants.subscriptionStatus.DURATION_NONE }
                }).exec();
            }else{      //validity is 0 days but month is already 0
                resolve(walletData);

                return promiseToCheckValidity;
            }
        });

        return Promise.all([promiseToCheckValidity, promiseToUpdateSubscriptionMonths]);
    }catch(e){
        console.log(e);
    }
};

generateToken = function(timestamp){
    let secretKeyTimestamp = constants.portwalletConnection.SECRET_KEY + timestamp;

    return md5(secretKeyTimestamp);
};

//Ratul - end