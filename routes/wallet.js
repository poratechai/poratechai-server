const express = require('express');
const router = express.Router();

const walletFunctionImported = require('../functions/wallet');

router.get('/checkPending', walletFunctionImported.checkPending);

router.post('/createInvoice', walletFunctionImported.createInvoice);

router.get('/getInvoice', walletFunctionImported.getInvoice);

//V1 available
router.get('/updateWallet', walletFunctionImported.updateWalletV2);

router.delete('/deleteInvoice', walletFunctionImported.cancelInvoice);

router.get('/payment_thankyou', (req, res) => { res.sendFile('payment_thankyou.html', { root: './views' }); });

module.exports = router;