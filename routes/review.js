const express = require('express');
const router = express.Router();

const reviewFunctionImported = require('../functions/review');

router.get('/getReviewsByUserIdForParent', reviewFunctionImported.getReviewsByUserIdForParent);

router.get('/getReviewsByUserIdForTutor', reviewFunctionImported.getReviewsByUserIdForTutor);

router.post('/reviewByParent', reviewFunctionImported.reviewByParent);

router.post('/reviewByTutor', reviewFunctionImported.reviewByTutor);

router.put('/updateReviewByParent', reviewFunctionImported.updateReviewByParent);

router.put('/updateReviewByTutor', reviewFunctionImported.updateReviewByTutor);

module.exports = router;