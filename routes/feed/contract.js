const express = require('express');
const router = express.Router();

const contractFunctionImported = require('../../functions/contract');

//V1 available
router.put('/sendContractReviewRequest', contractFunctionImported.sendContractReviewRequestV2);

router.put('/acceptContractReviewRequest', contractFunctionImported.acceptContractReviewRequest);

router.put('/acceptTutor', contractFunctionImported.acceptTutor);

router.put('/cancelJobinPhase0', contractFunctionImported.cancelJobinPhase0);

router.put('/cancelJobinPhase1', contractFunctionImported.cancelJobinPhase1);

router.put('/cancelJobinPhase2', contractFunctionImported.cancelJobinPhase2);

router.put('/cancelJobinPhase3', contractFunctionImported.cancelJobinPhase3);

router.put('/cancelJobinPhase4', contractFunctionImported.cancelJobinPhase4);

router.put('/parentCancelsJob', contractFunctionImported.parentCancelsJob);

module.exports = router;