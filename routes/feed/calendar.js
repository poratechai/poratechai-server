const express = require('express');
const router = express.Router();

const calendarFunctionImported = require('../../functions/calendar');

router.get("/getJobCalendarMonth", calendarFunctionImported.getJobCalendarMonth);

router.post("/parentStudentSignature", calendarFunctionImported.parentStudentSignature);

router.post("/tutorSignature", calendarFunctionImported.tutorSignature);

router.post("/parentStudentNote", calendarFunctionImported.parentStudentNote);

router.post("/tutorNote", calendarFunctionImported.tutorNote);

module.exports = router;