const express = require('express');
const router  = express.Router();

const jobFunctionImported = require('../../functions/job');

//For parents/students - start
router.post('/', jobFunctionImported.saveJob);

router.put('/', jobFunctionImported.updateJob);

router.delete('/', jobFunctionImported.deleteJob);

router.get('/singlePostedJob', jobFunctionImported.getSinglePostedJobAndMatches);

router.get('/postedJobs', jobFunctionImported.getPostedJobsAndMatches);

router.get('/singleActivePostedJob', jobFunctionImported.getSingleActivePostedJob);

router.get('/activePostedJobs', jobFunctionImported.getActivePostedJobs);

router.get('/singlePostedJobHistory', jobFunctionImported.getSinglePostedJobHistory);

router.get('/postedJobHistory', jobFunctionImported.getPostedJobHistory);
//For parents/students - end

//For tutors - start
router.get('/singleMatchedJob', jobFunctionImported.getSingleMatchedJob);

router.get('/matchedJobs', jobFunctionImported.getMatchedJobs);

router.get('/singleAppliedJob', jobFunctionImported.getSingleAppliedJob);

router.get('/appliedJobs', jobFunctionImported.getAppliedJobs);

router.get('/singleActiveJob', jobFunctionImported.getSingleActiveJob);

router.get('/activeJobs', jobFunctionImported.getActiveJobs);

router.get('/singleJobHistory', jobFunctionImported.getSingleJobHistory);

router.get('/jobHistory', jobFunctionImported.getJobHistory);
//For tutors - end

module.exports = router;