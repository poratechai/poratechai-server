const express = require('express');
const router = express.Router();

const statsFunctionImported = require('../functions/stats');

router.get('/tutor/', statsFunctionImported.getTutorStats);

router.get('/parent/', statsFunctionImported.getParentStats);

module.exports = router;