const express = require('express');
const router = express.Router();

const profile = require('../../functions/profile');
const update = require('../../functions/update');

router.put('/setTutorOrParent', profile.setTutorOrParent);

router.get('/isVerifiedByVerificationImages', profile.checkVerificationStatus);

router.get('/', profile.getProfile);

router.get('/others', profile.getProfileOthers);

module.exports = router;