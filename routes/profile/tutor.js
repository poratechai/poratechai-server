const express = require('express');
const router = express.Router();

const userFunctionImported = require('../../functions/user');

//UPDATE - TUTOR - start
router.put('/updateInstitutions', userFunctionImported.updateInstitutions);

router.put('/updateVerificationImages', userFunctionImported.updateVerificationImages);

router.put('/updateSkills', userFunctionImported.updateSkills);

router.put('/updateClasses', userFunctionImported.updateClasses);

router.put('/updateSubjects', userFunctionImported.updateSubjects);

router.put('/updateLocation', userFunctionImported.updateLocation);

router.put('/updateDaysPerWeek', userFunctionImported.updateDaysPerWeek);

router.put('/updateSalary', userFunctionImported.updateSalary);
//UPDATE - TUTOR - end

module.exports = router;