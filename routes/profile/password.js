const express = require('express');
const router = express.Router();

const password = require('../../functions/password');

router.put('/changePassword', password.changePasswordRouterFunction);

router.post('/resetPassword', password.resetPasswordRouterFunction); //Postponed - need to remove :id - use req.headers instead

module.exports = router;