const express = require('express');
const router = express.Router();

const picture = require('../../functions/picture');

router.put('/uploadProfilePicture', picture.uploadProfilePicture);

router.get('/getProfilePicture', picture.getProfilePicture);

router.put('/uploadCredentialPicture', picture.uploadCredentialPicture);

router.get('/getCredentialPictures', picture.getCredentialPictures);

router.delete('/deleteCredentialPicture', picture.deleteCredentialPicture);

module.exports = router;