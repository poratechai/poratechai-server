const express = require('express');
const router = express.Router();

const matchFunctionImported = require('../functions/match');

router.use(function timeLog (req, res, next) {
  console.log('Time: ', new Date());
  next();
});

router.get('/', (req, res) => res.end('Welcome to Porate Chai !'));

router.post('/createDummyMatch', matchFunctionImported.createDummyMatchHeroku);

module.exports = router;
