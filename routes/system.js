const express = require('express');
const router  = express.Router();

const readSystemConstantsImported = require('../functions/readSystemConsts');
const userFunctionImported = require('../functions/user');
const peekFunctionImported = require('../functions/peek');

/**
 * @api {get} /routes/system/privacyPolicy Request privacy policy
 * @apiGroup System
 *
 * @apiName Get Privacy Policy
 * @apiDescription This endpoint hosts the privacy policy file for Porate Chai
 *
 * @apiSuccess {HTML} privacyPolicyPage Web page for privacy policy
 */
router.get('/privacyPolicy', (req, res) => { res.sendFile('privacy_policy.html', { root: './views' });});

/**
 * @api {get} /routes/system/androidVersionCode Request Latest Android Version Code
 * @apiGroup System
 *
 * @apiName Get Android Version Code
 * @apiDescription The android version code sent from this endpoint, represents the version of the Android app which
 * contains breaking changes. This version of the app is already updated in the PlayStore. This API call from the
 * Android app enforces users to update their app to the latest version.
 *
 * @apiSuccess {String} androidVersionCode Latest Android Version Code of PlayStore
 */
router.get('/androidVersionCode', readSystemConstantsImported.readAndroidVersion);

/**
 * @api {get} /routes/system/checkConnectivity Request Connectivity Status
 * @apiGroup System
 *
 * @apiName Get App to Server connectivity status
 * @apiDescription This endpoint responds to a dummy request from the app to ensure connectivity
 *
 * @apiSuccess {String} Message to display to the user upon successful connectivity
 */
router.get('/checkConnectivity', readSystemConstantsImported.checkConnectivity);

/**
 * @api {get} /routes/system/systemArrayConstants Request System Constants
 * @apiGroup System
 *
 * @apiName Get System Constants
 * @apiDescription This endpoint sends the classesToTeach, subCategories and subjects array/json constants of Porate Chai
 *
 *
 * @apiSuccess {JSON} constants System constants in JSON format
 *
 * @apiSuccess {String[]} constants.classesToTeach String array of classesToTeach constants
 *
 * @apiSuccess {JSON} constants.subcategories Subcategories constants in JSON Format with respect to multiple categories
 * @apiSuccess {String[]} constants.subcategories.Academics String array of subCategories constants of 'Academic' category
 * @apiSuccess {String[]} constants.subcategories.Music String array of subCategories constants of 'Music' category
 * @apiSuccess {String[]} constants.subcategories.Language String array of subCategories constants of 'Language' category
 * @apiSuccess {String[]} constants.subcategories.IT String array of subCategories constants of 'IT' category
 * @apiSuccess {String[]} constants.subcategories.Cooking String array of subCategories constants of 'Cooking' category
 * @apiSuccess {String[]} constants.subcategories.Sports String array of subCategories constants of 'Sports' category
 * @apiSuccess {String[]} constants.subcategories.FineArts String array of subCategories constants of 'FineArts' category
 * @apiSuccess {String[]} constants.subcategories.Driving String array of subCategories constants of 'Driving' category
 * @apiSuccess {String[]} constants.subcategories.Religion String array of subCategories constants of 'Religion' category
 * @apiSuccess {String[]} constants.subcategories.Dancing String array of subCategories constants of 'Dancing' category
 *
 * @apiSuccess {String[]} constants.subjects String array of Academic subjects
 *
 *
 * @apiSuccessExample {JSON} Success response:
 *  HTTP/1.1 200 OK
 *  {
 *  "classesToTeach":[
 *      "Primary", "Secondary", "Higher Secondary", "Admission"
 *  ],
 *  "subcategories":{
 *      "Academics":[
 *          "Cambridge", "Edexcel", "IB", "GRE", "SAT", "American", "Australian", "National Bangla Medium", "National English Version", "Madrasa"
 *      ],
 *      "Music":[
 *          "Guitar", "Vocal", "Drums", "Keyboard", "Tabla", "Flute", "Harmonium", "Violin", "Ukulele"
 *      ],
 *      "Languages":[
 *          "English", "Bangla", "French", "Spanish", "German", "Chinese", "Arabic", "Hindi"
 *      ],
 *      "IT":[
 *          "Programming", "SEO", "Graphics Designing", "Web Development", "Video Editing"
 *      ],
 *      "Cooking":[
 *          "Bangla", "Indian", "Thai", "Chinese", "Continental", "Baking"
 *      ],
 *      "Sports":[
 *          "Football", "Basketball", "Cricket", "Lawn tennis", "Table tennis", "Archery", "Chess", "Badminton", "Swimming", "Cycling", "Billiard", "Fitness"
 *      ],
 *      "Fine Arts":[
 *          "Drawing and Painting", "Sculpture"
 *      ],
 *      "Driving":[
 *          "Car", "Motorcycle"
 *      ],
 *      "Religion":[
 *          "Islam", "Christianity", "Buddhism", "Hinduism"
 *      ],
 *      "Dancing":[
 *          "Classical", "Salsa", "Hiphop", "Tango"
 *      ]
 *  },
 *  "subjects":[
 *      "English", "English Literature", "Bangla", "General Science", "Mathematics", "Further/Higher Mathematics",
 *       "Physics", "Chemistry", "Biology", "Economics", "Accounting", "Business Studies", "Commerce", "Geography",
 *       "History", "ICT", "Social Science", "Religious Studies", "Bangladesh and Global Studies"
 *  ]
 * }
 */
router.get('/systemArrayConstants', readSystemConstantsImported.readFile);

router.get('/getPeekResults', peekFunctionImported.getPeekResults);

//V1 available
/**
 * @api {get} /routes/system/systemWalletConstants Request System Subscription Constants
 * @apiGroup System
 *
 * @apiName Get System Subscription Constants
 * @apiDescription This endpoint returns the payment subscription constants in the form of a JSON
 *
 * @apiSuccess {JSON} subscriptionConstants JSON object containing the subscription constants
 * @apiSuccess {String} subscriptionConstants.DURATION_NONE Subscription duration when not subscribed
 * @apiSuccess {String} subscriptionConstants.DURATION_ONE_MONTH Subscription duration when subscribed for one month
 * @apiSuccess {String} subscriptionConstants.DURATION_THREE_MONTHS Subscription duration when subscribed for three months
 * @apiSuccess {String} subscriptionConstants.COST_ONE_MONTH Subscription cost for a one month subscription
 * @apiSuccess {String} subscriptionConstants.COST_THREE_MONTHS Subscription cost for three months subscription
 *
 * @apiSuccessExample {JSON} Success Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "DURATION_NONE": 0,
 *      "DURATION_ONE_MONTH": 1,
 *      "DURATION_THREE_MONTHS": 3,
 *      "COST_ONE_MONTH" : 1000,
 *      "COST_THREE_MONTHS" : 2000
 *  }
 */
router.get('/systemWalletConstants', readSystemConstantsImported.readSystemWalletConstantsV2);

/**
 * @api {get}
 * @apiGroup System
 *
 * @apiName Get Support Number
 * @apiDescription This endpoint sends the support number for Porate Chai users
 *
 * @apiSuccess {String} supportNumber Porate Chai Support Number
 */
router.get('/supportNumber', readSystemConstantsImported.readSupportNumber);

router.put('/verifyUserFromAdminApp', userFunctionImported.updateVerificationFromAdminApp);

module.exports = router;