const express = require('express');
const router = express.Router();

const couponImported = require('../functions/coupon');

router.get('/getPrice', couponImported.getPriceForUser);

router.post('/createCoupon', couponImported.createCoupon);

router.put('/addCouponToUser', couponImported.addCouponToUser);

router.get('/getCoupons', couponImported.getCoupons);

router.get('/getCouponsForDeletion', couponImported.getCouponsForDeletion);

router.delete('/deleteCoupons', couponImported.deleteCoupons);

module.exports = router;