const express = require('express');
const router = express.Router();

const register = require('../functions/register');
const login = require('../functions/login');
const logout = require('../functions/logout');
const smsVerification = require('../functions/smsVerification');

router.post('/login', login.routerFunction);

router.get('/userExists', login.userExists);

router.post('/register', register.routerFunction);

router.put('/verifyCode', smsVerification.verifyCode);

router.post('/resendCode', smsVerification.resendCode);

router.put('/logout', logout.logoutUser);

module.exports = router;
