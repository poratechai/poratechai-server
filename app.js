const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const morgan = require('morgan');
const winston = require('./config/winston');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const sassMiddleware = require('node-sass-middleware');
const schedule = require('node-schedule');

const routes = require('./routes/routes');
const app = express();

// setup mongoose database
{
    const { connectToMlabDatabase } = require('./functions/dbMongoose');
    connectToMlabDatabase();
}

// setup middleware and environment
{
    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'hbs');

    // uncomment after placing your favicon in /public
    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    // app.use(morgan('combined', { stream: winston.stream }));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cookieParser());
    app.use(sassMiddleware({
        src: path.join(__dirname, 'public'),
        dest: path.join(__dirname, 'public'),
        indentedSyntax: true, // true = .sass and false = .scss
        sourceMap: true
    }));
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(express.static(path.join(__dirname, 'images')));

    // error handler
    app.use(function(err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });
}

// setup routes
{
    let routesSystem            = require('./routes/system');

    let routesAuthentication    = require('./routes/authentication');

    let routesProfile           = require('./routes/profile/profile');
    let routesProfilePassword   = require('./routes/profile/password');
    let routesProfilePicture    = require('./routes/profile/picture');

    let routesProfileTutor      = require('./routes/profile/tutor');
    let routesProfileParent     = require('./routes/profile/parent');

    let routesWallet            = require('./routes/wallet');
    let routesCoupon            = require('./routes/coupon');
    let routesStats             = require('./routes/stats');
    let routesReview            = require('./routes/review');

    let routesJob               = require('./routes/feed/job');
    let routesContract          = require('./routes/feed/contract');
    let routesCalendar          = require('./routes/feed/calendar');

    app.use('/',                    routes);
    app.use('/system',              routesSystem);

    app.use('/authentication',      routesAuthentication);

    app.use('/profile',             routesProfile);
    app.use('/profile/password',    routesProfilePassword);
    app.use('/profile/picture',     routesProfilePicture);

    app.use('/profile/tutor',       routesProfileTutor);
    app.use('/profile/parent',      routesProfileParent);

    app.use('/wallet',              routesWallet);
    app.use('/coupon',              routesCoupon);
    app.use('/stats',               routesStats);
    app.use('/review',              routesReview);

    app.use('/feed/job',            routesJob);
    app.use('/feed/contract',       routesContract);
    app.use('/feed/calendar',       routesCalendar);
}

// initiate matching client
{
    const { initiateClient } = require('./functions/matching/matching_client');
    initiateClient();
}

// initiate generation and caching of peek results
{
    // const { generatePeekResults } = require('./functions/scheduled');
    // generatePeekResults();
}

// schedule jobs
{
    const { optimizeMatchModelV2, checkNewMatches, generatePeekResults } = require('./functions/scheduled');

    //run after every 03 seconds
    schedule.scheduleJob('03 * * * * *', function(fireDate){
        console.log(fireDate);
        optimizeMatchModelV2();    //for eliminating matching documents that are 60 days old or are in active phase
        //scheduled.runPythonMatching();
        checkNewMatches();         //for sending notifications
    });

    //run after every 10 seconds
    schedule.scheduleJob('00 00 03 * * *', function(fireDate){
        generatePeekResults();
    });
}

module.exports = app;
