const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const smsCodeSchema = new Schema({
  userId: { type : Schema.Types.ObjectId, ref: 'User' },
  code: { type: String}
});

module.exports = mongoose.model('SmsCode', smsCodeSchema);
