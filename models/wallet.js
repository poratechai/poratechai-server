const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const walletSchema = new Schema({
    userId              :   { type: Schema.Types.ObjectId, unique: true, index: true, ref : 'User' },
    contractBalance     :   { type: Number, default : 0 },
    subscriptionMonths  :   { type: Number, default : 3 },
    transactions        :   [{ type: Schema.Types.ObjectId, ref: 'Transaction' }],
    lastPaidDate        :   { type: Schema.Types.Date, default: Date.now() }
});

module.exports = mongoose.model('Wallet', walletSchema);