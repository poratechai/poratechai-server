const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const errorSchema = new Schema({
  createdAt : { type: Date, default: Date.now()},
  errorMessage: { type: String },
  route : { type: String },
  params: { type: Object } ,
})

module.exports = mongoose.model('Error', errorSchema)