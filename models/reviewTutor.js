var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator')

//Any changes to this model reflect in the reviewParent model

var schema = new Schema(
    {
      userId        : {type: Schema.Types.ObjectId},
      reviewerName  : {type: String, default: ""},
      rating        : {type: Number, default: 0},
      reviewTags    : [{type: String, default: ""}],
      updatedAt     : {type: Schema.Types.Date, default: Date.now()}
    }
);
schema.plugin(uniqueValidator);

module.exports = mongoose.model('ReviewTutor', schema, 'reviewsTutor');
