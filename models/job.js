let mongoose = require('mongoose');
let Schema = mongoose.Schema;

//indexing - category, subjects, location
let schema = new Schema(
    {
        //job data
        jobTitle              : { type : String, default : " "},
        category              : { type : String, default : " ", index : true},
        subcategory           : { type : String, default : " "},
        class                 : { type : String, default : " "},
        subjects              : { type : [ { type : String, default : " " } ], index : true},
        tutorGenderPreference : { type : String},
        locationLat           : { type : Number, default : 0},
        locationLong           : { type : Number, default : 0},
        location              : { type : String, default : " "},
        salary                : { type : Number, default : 0},
        daysPerWeek           : { type : Number, default : 0},
        status                 : { type : Number, default : 0},

        //job references
        userId                : { type : Schema.Types.ObjectId },
        tutorId               : { type : Schema.Types.ObjectId },

        //reviews
        reviewIdParent        : { type : Schema.Types.ObjectId },
        reviewIdTutor         : { type : Schema.Types.ObjectId },

        //date
        createdAt             : { type : Schema.Types.Date, default : Date.now() },
        updatedAt             : { type : Schema.Types.Date, default : Date.now() }
    },
    {
        minimized : false,
        toJSON    : { virtuals : true }
    }
);
//mongoose virtuals for improved population of data in job feed
schema.virtual('userProfile', {
        ref             :       'User',
        localField      :       'userId',
        foreignField    :       '_id'
});

schema.virtual('tutorProfile', {
        ref             :       'User',
        localField      :       'tutorId',
        foreignField    :       '_id'
});

schema.virtual('matchedTutors', {
        ref             :       'match',
        localField      :       '_id',
        foreignField    :       'jobId'
});

schema.virtual('reviewTutor', {
        ref             :       'ReviewTutor',
        localField      :       'reviewIdTutor',
        foreignField    :       '_id'
});

schema.virtual('reviewParent', {
        ref             :       'ReviewParent',
        localField      :       'reviewIdParent',
        foreignField    :       '_id'

});

module.exports = mongoose.model('Job', schema);
