let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let calendarSchema = new Schema(
    {
        jobId       : { type: Schema.Types.ObjectId },

        dates       : [{
            parentStudentSignature  : { type: Boolean, default: false },
            tutorSignature          : { type: Boolean, default: false },
            parentStudentNote       : { type: String, default: " " },
            tutorNote               : { type: String, default: " " },
            year                    : { type: Number, default: 0 },
            month                   : { type: Number, default: 0 },
            day                     : { type: Number, default: 0 }
        }],

        createdAt   : { type: Schema.Types.Date, default: Date.now() }
    }
);

//mongoose virtuals for improved population of data in calendar
calendarSchema.virtual('jobDetails', {
    ref             : 'Job',
    localField      : 'jobId',
    foreignField    : '_id'
});

module.exports = mongoose.model('Calendar', calendarSchema);