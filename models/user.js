const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const userSchema = new Schema({
  fullName 	                      : { type : String, default : " "},
  gender                          : { type : String, default : " "},
  locationLat                     : { type : Number, default : 0},
  locationLong	                  : { type : Number, default : 0},
  location                        : { type : String, default: " "},
  radius 	                      : { type : Number, default : 0},
  minPreferredSalary              : { type : Number, default : 0},
  contactNumber                   : { type : Number, unique: true, required: true},
  // email		                      : { type : String, required: false, index: { unique: true, partialFilterExpression: {email: {$type: "string"}}}, default: null},
  email		                        : { type : String, default: " "},
  hashedPassword                  : { type : String, default : " "},
  deviceIdToken                   : { type : String, default: ""},
  isTutor                         : { type : String, default : ""},
  isVerified                      : { type : String, default : false}, //SMS verification
  isVerifiedByVerificationImages  : { type : String, default: false},  //Manual verification from admin app
  tutorUpdateFlag                 : { type : String, default : false},
  profilePicture                  : { type : String, default : null},
  credentialPictures              : [{ type : String, default : null}],
  institution                     : [{ type : String, default : " "}],
  preferredSubjects               : [{ type : String, default : " "}],
  preferredCategory               : [{ type : String, default : " "}],
  preferredSubcategory            : [{ type : String, default : " "}],
  preferredClassToTeach           : [{ type : String, default : " "}],
  preferredDaysPerWeek            : { type : Number, default : 0},
  createdAt	                      : { type : String, default : " "},
  tempPassword	                  : { type : String, default : " "},
  tempPasswordTime                : { type : String, default : " "},
  postedJobData                   : [ {type: Schema.Types.ObjectId, ref: 'Job'} ],
  jobHistory                      : [ {type: Schema.Types.ObjectId, ref: 'Job'} ],
  walletId                        : { type: Schema.Types.ObjectId, ref: 'Wallet' },
  couponId                        : { type: Schema.Types.ObjectId, ref: 'Coupon' }
});

module.exports = mongoose.model('User', userSchema);
