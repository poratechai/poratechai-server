let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let couponSchema = new Schema(
    {
        couponTitle         : { type : String, default: " "},
        couponDescription   : { type : String, default: " "},

        oneMonth            : { type : Number, default: 1000 },
        threeMonths         : { type : Number, default: 2000 },

        createdAt           : { type : Schema.Types.Date, default: Date.now() }
    }
);

module.exports = mongoose.model('Coupon', couponSchema);