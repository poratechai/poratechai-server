
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema(
    {
        tutorId            : { type : mongoose.Schema.Types.ObjectId},
        jobId              : { type : mongoose.Schema.Types.ObjectId},
        previousStatus     : { type : Number, default : 0},
        currentStatus      : { type : Number, default : 0},
        daysPerWeek        : { type : Number, default : 0},
        salaryOffer        : { type : Number, default : 0},
        message            : { type : String, default: ""},
        dateOfStatusChange : { type : Schema.Types.Date, default : Date.now() },
        notified           : { type : Number, default : 0}
    },
    {
        toJSON : { virtuals : true }
    });
//mongoose virtuals
schema.virtual('tutorProfile',{
        ref             :       'User',
        localField      :       'tutorId',
        foreignField    :       '_id'
});

schema.virtual('jobData',{
        ref             :       'Job',
        localField      :       'jobId',
        foreignField    :       '_id'
});

module.exports = mongoose.model('match', schema);
