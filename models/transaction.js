const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const transactionSchema = new Schema({
    userId          :   { type: Schema.Types.ObjectId, ref: 'User', index: true },
    invoiceNumber   :   { type: String },
    amount          :   { type: Number, default: 0 },
    oneMonthCost    :   { type: Number, default: 1000 },
    threeMonthsCost :   { type: Number, default: 2000 },
    updateDate      :   { type: Schema.Types.Date, default: Date.now() },
    status          :   { type: Number, default: 0 }
});

module.exports = mongoose.model('Transaction', transactionSchema);